#include <iostream>

struct C {
    C() {}
    C(const C&) { std::cout << "A copy was made.\n"; }
};

C f(bool cond = false) {
    C class1;
    C class2;
    return cond ? class1 : class2;
}

int main() {
    std::cout << "Hello World!\n";
    C obj = f();
}

