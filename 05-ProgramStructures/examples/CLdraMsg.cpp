#include "CLdraMsg.h"

CLdraMsg * CLdraMsg::cldraMsg_m = 0;

CLdraMsg * CLdraMsg::getInstance()
{
    if (cldraMsg_m == 0)
    {
        static CLdraMsg cldraMsg_m;
    }
    return cldraMsg_m;
}

int main()
{
    CLdraMsg::getInstance();

    return 0;
}

