#pragma once

#include "MacAddress.h"

class Device {
public:
    virtual ~Device();
    Device(MacAddress& srcMac);

    const MacAddress& getMacAddress() const;
    static unsigned getNumberOfDevices();

private:
    MacAddress& m_sourceMacAddress;

    static unsigned m_numberDevices;
};
