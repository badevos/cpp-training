#include "Device.h"

unsigned Device::m_numberDevices = 0;

Device::Device(MacAddress& macAddress)
    : m_sourceMacAddress(macAddress)
{
    ++m_numberDevices;
}

Device::~Device()
{
    --m_numberDevices;
}


const MacAddress& Device::getMacAddress() const
{
    return m_sourceMacAddress;
}


unsigned Device::getNumberOfDevices() {
    return m_numberDevices;
}
