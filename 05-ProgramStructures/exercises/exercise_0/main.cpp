#include "MacAddress.h"
#include "Client.h"
#include "Server.h"

#include <iostream>


int main()
{
    MacAddress serverMacAddress(0x01, 0, 0, 0, 0, 0xF0);
    std::cout << "MAC address of the server = " << serverMacAddress.toString() << "\n";

#if MOVE_LINE_DOWN_STEP_BY_STEP
    Server server(serverMacAddress);

    MacAddress defaultMacAddress = Client::getDefaultMacAddress();
    Client client(defaultMacAddress);
    std::cout << "Default MAC address for a client = " << defaultMacAddress.toString() << "\n";

    std::cout << "Client MAC address " << client.getMacAddress().toString() << ", should be equal to the default."<< "\n";
#endif
    std::cout << "Number of devices: " << Device::getNumberOfDevices() << "\n";

    return 0;
}

