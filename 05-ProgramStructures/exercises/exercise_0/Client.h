#pragma once

#include "Device.h"
#include "MacAddress.h"


class Client : public Device {
public:
    Client(MacAddress& sourceMac);

    static MacAddress getDefaultMacAddress() {
        MacAddress defaultAddr(0x1E, 0, 0, 0, 0, 0x01);
        return defaultAddr;
    }
};
