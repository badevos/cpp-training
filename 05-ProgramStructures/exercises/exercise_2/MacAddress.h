#pragma once

#include <stdint.h>
#include <string>

namespace DataLink {

class MacAddress {
public:
  MacAddress(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3, uint8_t b4,  uint8_t b5);
  MacAddress(const MacAddress&);
  ~MacAddress();

  const std::string toString() const;

private:
  uint8_t* mac;
  static const unsigned macAddressLength_c = 6;
};

}
