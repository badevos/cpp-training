#pragma once

#include "Device.h"
#include "MacAddress.h"

namespace XDevices {

class Client : public Device {
public:
    Client(DataLink::MacAddress& sourceMac);

    static const DataLink::MacAddress getDefaultMacAddress() {
        DataLink::MacAddress defaultAddr(0x1E, 0, 0, 0, 0, 0x01);
        return defaultAddr;
    }
};

}
