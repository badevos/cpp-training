#include "MacAddress.h"
#include "Client.h"
#include "Server.h"

#include <iostream>


int main()
{
    DataLink::MacAddress serverMacAddress(0x01, 0, 0, 0, 0, 0xF0);
    std::cout << "MAC address of the server = " << serverMacAddress.toString() << '\n';

    XDevices::Server server(serverMacAddress);

    DataLink::MacAddress defaultMacAddress = XDevices::Client::getDefaultMacAddress();
    XDevices::Client client(defaultMacAddress);
    std::cout << "Default MAC address for a client = " << defaultMacAddress.toString() << '\n';

    std::cout << "Client MAC address " << client.getMacAddress().toString() << ", should be equal to the default."<< '\n';

    std::cout << "The number of devices is " << XDevices::Device::getNumberOfDevices() << '\n';

    return 0;
}

