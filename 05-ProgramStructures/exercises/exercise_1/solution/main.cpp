#include <fstream>
#include <sstream>

#include "LogEntry.h"


int main(int argc, char** argv)
{
    if (argc < 2) return -1; // A return of -1 denotes an error condition.

    /// IMPLEMENT HERE
    /// Create string from all the char arrays (max == argc) in the argv (except the filename == argv[0])
    std::ostringstream logData;
    for (int i = 1; i < argc; i++) {
        logData << argv[i] << ' ';
    }

    LogEntry logEntry(logData.str());

    std::clog << logEntry << std::endl; // standard error

    /// IMPLEMENT HERE
    /// 1. Open/create the logfile, return error if failed
    /// 2. Append the logEntry to the log file
    /// 3. Close the file.
    std::ofstream logfile("logfile", std::ios::app); // ios::app -> Seek to the end before each write.

    if (!logfile)  return -1; // check for errors when opening the file

    logfile << logEntry << std::endl;
    logfile.close();

    return 0;
}


