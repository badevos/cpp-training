#include "LogEntry.h"

#include <iostream>
#include <ctime>
#include <sstream>
#include <iomanip>


using namespace std;


namespace {

/**
 * Append a time entry to a ostream with width 2, fill with zeros.
 * @param os The stream to append the time entry to
 * @param timeEntry  time type integer
 * @return the same stram with appended time entry
 */
ostream& fillWithZeros(ostream& os, const int timeEntry)
{
    os << std::setfill('0') << std::setw(2) << timeEntry;
    return os;
}


/**
 * @return current time as string
 */
string timestamp()
{

    time_t rawtime;
    time(&rawtime); // returns the current calender time

    tm * timeinfo = localtime(&rawtime); // express uers's local time

    /// IMPLEMENT HERE
    /// Make a ostringstream with the represented time
    /// Follow the included header /usr/include/c++/4.8.2/ctime -> /usr/include/time.h -> "tm"

    ostringstream stream;
    // Append all with fixed length (can be improved by implementing an operator<<, seen in session 7 of the course)
    fillWithZeros(stream, (timeinfo->tm_year + 1900) % 100); // only 2 digits for the year
    fillWithZeros(stream << "-", timeinfo->tm_mon);
    fillWithZeros(stream << "-", timeinfo->tm_mday);
    fillWithZeros(stream << ":", timeinfo->tm_hour);
    fillWithZeros(stream << ":", timeinfo->tm_min);
    fillWithZeros(stream << ":", timeinfo->tm_sec);
//
    return stream.str();
}

}

/**
 * Constructor for Logger
 * @param dataStr the actual data to print after the timestamp
 */
LogEntry::LogEntry(string dataStr) : dataString(dataStr), timeString(timestamp())
{
}


/**
 * @param ost  output stream
 * @param ls  the current logger
 * @return returns the same stream with time and logger data
 */
ostream& operator<<(ostream& os, const LogEntry& logger)
{
    /// IMPLEMENT HERE
    /// Append to the ost string a logger prefix like:
    /// ~|2017-04-06 10:51| my text |~

    os << "~|" << logger.timeString << "| " << logger.dataString << " |~";
    return os;
}


