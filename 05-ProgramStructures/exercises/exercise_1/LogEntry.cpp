#include "LogEntry.h"

#include <iostream>
#include <ctime>
#include <sstream>
#include <iomanip>


using namespace std;


namespace {

ostream& fillWithZeros(ostream& os, const int timeEntry)
{
    os << std::setfill('0') << std::setw(2) << timeEntry;
    return os;
}


/**
 * @return current time as string
 */
string timestamp()
{

    time_t rawtime;
    time(&rawtime); // returns the current calender time

    tm * timeinfo = localtime(&rawtime); // express uers's local time

    /// IMPLEMENT HERE
    /// 1. Make a ostringstream with the represented time
    /// 2. Follow the included header /usr/include/c++/4.8.2/ctime -> /usr/include/time.h -> "tm"
    /// 3. return the string from the stringstream
//
    return ostringstream(); //TODO: replace empty string by time string
}

}

/**
 * Constructor for Logger
 * @param dataStr the actual data to print after the timestamp
 */
LogEntry::LogEntry(string dataStr) : dataString(dataStr), timeString(timestamp())
{
}


/**
 * @param ost  output stream
 * @param ls  the current logger
 * @return returns the same stream with time and logger data
 */
ostream& operator<<(ostream& os, const LogEntry& logger)
{
    /// IMPLEMENT HERE
    /// Append to the ost string a logger prefix like:
    /// ~|2017-04-06 10:51| my text |~

    return os;
}


