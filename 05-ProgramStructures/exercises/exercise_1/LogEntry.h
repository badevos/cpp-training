#pragma once

#include <string>
#include <iostream>


class LogEntry {
public:

    LogEntry(std::string s);

    friend std::ostream& operator<<(std::ostream&, const LogEntry&);

private:
    std::string dataString;
    std::string timeString;

};
