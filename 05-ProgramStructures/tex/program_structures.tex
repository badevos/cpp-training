%------------------------------------------------

\title[Introduction]{Program Structures}
\maketitle

%------------------------------------------------

\makeoverview

%------------------------------------------------

\section{Passing Data Structures}
\makecurrentsection

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \begin{columns}[onlytextwidth, t]
        \begin{column}{.45\textwidth}
            \textbf{Pass by value}
            {\footnotesize
                \begin{itemize}
                    \item Parameters are \underline{input} only
                    \item Only used to pass default types or iterators
                \end{itemize}
            }
            
            \begin{lstlisting}
void swap1(int x, int y) 
{ 
    int temp = x;
    x = y;
    y = temp;
}
            \end{lstlisting}
            {\footnotesize
            \begin{tabular}{p{.8\textwidth}p{.2\textwidth}}
                Variables are swapped but go out of scope on return &   
                %TODO align correctly
                \begin{figure}			\includegraphics[width=.2\textwidth]{images/bad.png}  \end{figure}
            \end{tabular}
            }
        \end{column}
        \begin{column}{.47\textwidth}
            \textbf{Pass by pointer}
            {\footnotesize
                \begin{itemize}
                    \item Parameters are \underline{input/output}
                    \item Used to pass data structures without copying
                \end{itemize}
            }
            \begin{lstlisting}
void swap2(int * x, int * y) 
{ 
    int temp = *x;
    *x = *y;
    *y = temp;
}
            \end{lstlisting}
            {\footnotesize
            \begin{tabular}{p{.8\textwidth}p{.2\textwidth}}
                Variables are swapped and values are reflected on return &   
                %TODO align correctly
                \begin{figure}			\includegraphics[width=.2\textwidth]{images/good.png}  \end{figure}
            \end{tabular}
            }          
        \end{column}
    \end{columns}
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \begin{columns}[onlytextwidth, t]
        \begin{column}{.45\textwidth}
            \textbf{Pass by value}
            {\footnotesize
                \begin{itemize}
                    \item Parameters are \underline{input} only
                    \item Only used to pass default types or iterators
                \end{itemize}
            }
            
            \begin{lstlisting}
void swap1(int x, int y) 
{ 
    int temp = x;
    x = y;
    y = temp;
}
            \end{lstlisting}
            {\footnotesize
                \begin{tabular}{p{.8\textwidth}p{.2\textwidth}}
                    Variables are swapped but go out of scope on return &   
                    %TODO align correctly
                    \begin{figure}			\includegraphics[width=.2\textwidth]{images/bad.png}  \end{figure}
                \end{tabular}
            }
        \end{column}
        \begin{column}{.45\textwidth}
            \textbf{Pass by reference}
            {\footnotesize
                \begin{itemize}
                    \item Parameters are \underline{input/output}
                    \item Used to pass data structures without copying
                \end{itemize}
            }
            \begin{lstlisting}
void swap2(int & x, int & y) 
{ 
    int temp = x;
    x = y;
    y = temp;
}
            \end{lstlisting}
            {\footnotesize
                \begin{tabular}{p{.8\textwidth}p{.2\textwidth}}
                    Variables are swapped and values are reflected on return\\
                    Difference with pass by pointer:\\
                    $\bullet$ \textbf{pointer can return NULL}
                     &   
                    %TODO align correctly
                    \begin{figure}			\includegraphics[width=.2\textwidth]{images/good.png}  \end{figure}
                \end{tabular}
            }          
        \end{column}
    \end{columns}
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Constness $<$part2$>$ \\ Recap} \\
    \bigskip
    \textbf{Q1:} What are the differences between:
    \begin{itemize}
        \item \lstinline{T * const myVar}
        \item \lstinline{T const * myVar}
        \item \lstinline{const T * const myVar}
    \end{itemize}
    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Constness $<$part2$>$ \\ References} \\
    \bigskip
    \References cannot be reassigned \\
    So \&const does not exist (== \&)
    \begin{itemize}
        \item \lstinline{T & myVar}
        \item \sout{\lstinline{T & const myVar}}
        \item \lstinline{T const & myVar  (equal to "const T & myVar")}
        \item \sout{\lstinline{T const & const myVar}}
    \end{itemize}
    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Constness $<$part2$>$ \\ Const Argument} \\
    \bigskip
    \begin{itemize}
        \item Protection from being modified within \\
        \begin{lstlisting}
void swap(const int & x, const int & y) 
{ 
    int temp = x;
    x = y;
    y = temp;
}
        \end{lstlisting}
        {\color{red} $\rightarrow$ error: read-only variable is not assignable}
    \end{itemize}
    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Pass by const \dots} \\
    \bigskip
    \begin{itemize}
        \item Explicitly telling the compiler the parameter is input only \\
              $\rightarrow$ Can be the reason to precede const before primitive type parameters
        \item Automatically add const for setters, make it a good habit!
    \end{itemize}
    \begin{lstlisting}
    void Participant::setAddress(const Address& address)
    { 
        m_address = address;
    }
    \end{lstlisting}
    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Constness $<$part2$>$ \\ Return Parameter} \\
    \bigskip
    {\footnotesize
    \textbf{Problem}
    \begin{lstlisting}
unsigned & id = bootSafeRam.getId();
error:   ^ invalid initialization of non-const reference of type 'unsigned int&' from an rvalue of type 'unsigned int'
    \end{lstlisting}    
    \textbf{Solution} \medskip \\
    The problem is that a temporary object cannot be bound to a non-const reference.\\
    \lstinline{const unsigned & id = bootSafeRam.getId();} \\
    \textit{Note:} When a temporary object is bound to a const reference, then the life of the temporary object is extended!
    }
    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Constness $<$part2$>$} \\
    \bigskip
    \begin{tabular}{l l}
        \textbf{Q2} & How to define a member reference? \\
        & What could be the penalty? \\
        & \\
        \textbf{A} & Only possible to initialize in the initializer list\\
        & Only possible to initialize once. \\
    \end{tabular} \\
    \bigskip
    Is more an advantage, use when a link should never be changed.
    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Return Value Optimization (RVO)} \\
    \bigskip
    \begin{itemize}
        \item OR "Copy elision": Omit copy resulting from a return statement
        \item Compiler optimization
        \item Supported on most compilers
        \item No optimization when the function returns different named objects depending on the path of execution
    \end{itemize}
    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Return Value Optimization (RVO)} \\
    \bigskip
    \begin{columns}[onlytextwidth, t]
        \begin{column}{.60\textwidth}
            \begin{lstlisting}[basicstyle=\ttfamily\tiny\color{black}]
#include <iostream>

struct C 
{
    C() {}
    C(const C&) { std::cout << "A copy was made.\n"; }
};

C f() 
{
    return C();
}

int main() 
{
    std::cout << "Hello World!\n";
    C obj = f();
}
            \end{lstlisting}
        \end{column}
        \begin{column}{.40\textwidth}
            \begin{lstlisting}
$ g++ RVO.cpp -o RVO
$ ./RVO
Hello World!
            \end{lstlisting}
        \end{column}    
    \end{columns}
    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Return Value Optimization (RVO)} \\
    \bigskip
    \begin{columns}[onlytextwidth, t]
        \begin{column}{.60\textwidth}
            \begin{lstlisting}[basicstyle=\ttfamily\tiny\color{black}]
#include <iostream>

struct C 
{ 
    C() {}
    C(const C&) { std::cout << "A copy was made.\n"; }
};

C f(bool cond = false) 
{
    C class1;
    C class2;
    return cond ? class1 : class2;
}

int main() 
{
    std::cout << "Hello World!\n";
    C obj = f();
}
            \end{lstlisting}
        \end{column}
        \begin{column}{.40\textwidth}
            \begin{lstlisting}
$ g++ NoRVO.cpp -o NoRVO
$ ./NoRVO
A copy was made.
Hello World!
            \end{lstlisting}
        \end{column}    
    \end{columns}
    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Exercise 0} \\
    \bigskip
    See 05-ProgramStructures/exercises/exercise\_0   
    \begin{itemize}
        \item Start by executing: \\
        \lstinline{$ g++ main.cpp MacAddress.cpp}\\
        \item Take a look at the classes MacAddress, Server \& Client \\
        \item Implement base class Device and subclasses Server \& Client \\
        \item Additional Q: How many times should you see?: \\
        \lstinline{$ A copy of a MAC address was made}
    \end{itemize}
    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Passing Data Structures:}
    \textbf{Summary}
    \bigskip
    \begin{itemize}
        \item References: \textbf{Fixed} pointer with \textbf{nicer} syntax
        \item Use references when copying is \textbf{expensive}
        \item Pass as \textbf{const} to ensure the function doesn't modify the (object's) value
        \item Return by value =$>$ RVO
    \end{itemize}
    \vfill
\end{frame}

%------------------------------------------------

\section{Streams}
\makecurrentsection

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Streams}
    \begin{itemize}
        \item Series of characters
        \item Consistent interface $\rightarrow$ easy to use
        \item Works standard with built-in types\\
        for user defined types $\rightarrow$ overload \lstinline{operator<</>>} [session7]
        \item Works for any type of storage medium
    \end{itemize}
    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Streams: Hierarchy (std)}
    \bigskip \\
    'i' for input, 'o' for output\\
    's' for string, 'f' for file\\
    \begin{figure}				
        \includegraphics[width=\textwidth]{images/streams.png}  
    \end{figure}
    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Streams: Error Handling (std)}
    \bigskip \\
    \begin{itemize}
        \item Valid or not valid:
            \begin{lstlisting}
ifstream file("test.txt");
if (!file) 
{
    cout << "An error occurred opening the file" << endl;
}
            \end{lstlisting}        
        \item \lstinline{good()} returns true when everything is okay.
        \item \lstinline{bad()}	returns true when a fatal error has occurred.
        \item \lstinline{fail()} returns true after an unsuccessful stream operation like an unexpected type of input being encountered.
        \item \lstinline{eof()}	returns true when the end of file is reached.
    \end{itemize}
    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Streams: Options (std)}
    \bigskip \\
    \begin{tabular}{l l}
        Constant & Explanation \\
         \hline
        app & seek to the end of stream before each write \\
        binary & open in binary mode \\
        in & open for reading \\
        out & open for writing \\
        trunc & discard the contents of the stream when opening \\
        ate & seek to the end of stream immediately after open \\
    \end{tabular}
    \bigskip \\    
    \textbf{Example:}\\
    \lstinline{ofstream logfile("logfile", ios::app);}

    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Streams: Reading Bytes}
    \bigskip \\
    Example: easily manipulate streams at their lowest level: bytes 
    \bigskip \\
    \begin{lstlisting}[basicstyle=\ttfamily\tiny\color{black}]
#include <iostream>
#include <fstream>

int main() 
{
    std::ifstream inputFile("a.txt");
    
    //std::ios::trunc means that the output file will be overwritten if exists         
    std::ofstream outputFile("a.txt.copy", std::ios::trunc);
    
    if (inputFile && outputFile ) 
    {  
        outputFile << ifs.rdbuf(); 
    }
    return 0;
}
    \end{lstlisting}
    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Exercise\_1: Logger}
    \bigskip \\
    \textbf{See 05-ProgramStructures/exercises/exercise\_1}\\
    {\footnotesize    
        A program appends all given arguments into a file where each line includes eg.:\\
        \lstinline{~|2017-04-06 10:51| Argument1 Argument2 |~}\\
        See example log file \lstinline{logfile_example}
        \bigskip \\
        Implement all necessary streams:
        \begin{itemize}
            \item Append all arguments from command line into 1 ostringstream 
            \item Make a ostringstream with the represented time\\
            Find the members of the given \lstinline{tm} type through the header file \lstinline{ctime}
            \item Implement the content of the \lstinline{operator<<} to print a logger entry
            \item Write the logger entry line at the end of the logger file
        \end{itemize}
        
        Check the logfile after running the program using \lstinline{$ cat logfile}\\
        See more implementation notes in the code
    }
    \vfill
\end{frame}

%------------------------------------------------

\section{Internal/external linkage}
\makecurrentsection

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Linkage}
    
    \textit{Internal linkage}
    \begin{itemize}
        \item Everything only in scope of a translation unit.
    \end{itemize}

    \textit{External linkage}
    \begin{itemize}
        \item Things that exist beyond a particular translation unit.\\
            \textbf{== accessible through the whole program}, which is the combination of all translation units (or object files).\\
            Also known as “global”    
    \end{itemize}
    
    \vfill
\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Inline keyword}
    \bigskip
    \begin{columns}[onlytextwidth, t]
        \begin{column}{0.55\textwidth}
            Replace function definition on call\\
            + Like macro $\rightarrow$ fast\\
            + Linker will never complain \\
            \bigskip
            - Can make code size bigger\\
            - Can pollute your code\\
            - Breaks encapsulation\\
            - Only suggestion to compiler to inline\\
        \end{column}
        \begin{column}{0.44\textwidth}
            \begin{lstlisting}[basicstyle=\ttfamily\tiny\color{black}]
class Participant
{
    public:
    inline getId() const { return id; }
    
    ...
}
            \end{lstlisting}
        \end{column}
    \end{columns}
    \bigskip
    $\rightarrow$ Compiler will mostly inline implicitly. Use with caution!\\
    Only use on trivial methods

\end{frame}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Namespaces}
    
    \begin{columns}[onlytextwidth, t]
        \begin{column}{0.5\textwidth}
            \begin{itemize}
            \item To avoid conflicts
            \item Scope resolution operator ::
            \item Can be nested
            \item Not a good practice:\\
              \lstinline{using namespace Xdevices;}\\
              $\rightarrow$ keep scope small\\
              $\rightarrow$ Never in header files!\\
            \end{itemize}
        \end{column}
        \begin{column}{0.45\textwidth}
            \begin{lstlisting}[basicstyle=\ttfamily\tiny\color{black}]
// In .h files:
namespace XDevices 
{
    class Dev 
    {
        public:
        static print(const Dev& dev) { ... }
    }
}

// In .cpp files:
namespace XDevices 
{
    ...
}

// Outside namespace XDevices:
Xdevices::Dev dev1;
XDevices::Dev::print(dev1)
            \end{lstlisting}
        \end{column}
    \end{columns}

\end{frame}

%------------------------------------------------

\section{static \& inline keywords}
\makecurrentsection

%------------------------------------------------

\section{Namespaces \& ADL}
\makecurrentsection

%------------------------------------------------

\section{Casts}
\makecurrentsection

%------------------------------------------------
