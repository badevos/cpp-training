#include <iostream>
#include <cmath>

class ellipse; // forward declaration of class ellipse for use in circle
class circle {
    friend class ellipse;
    private:
        float m_diameter;
    public:
        circle(const float diameter) : m_diameter(diameter) {}
        float area() { return M_PI*(m_diameter/2)*(m_diameter/2); }
};

class ellipse {
    private:
        float m_major, m_minor;
    public:
        ellipse(const float major, const float minor) : m_major(major), m_minor(minor) {}
        ellipse(const circle &c) : m_major(c.m_diameter), m_minor(c.m_diameter) {}
        float area() { return M_PI*(m_major/2)*(m_minor/2); }
};

int main() {
    circle c(5);
    ellipse e(c);
    std::cout << "c.area = " << c.area() << " / e.area = " << e.area() << "\n";
}
