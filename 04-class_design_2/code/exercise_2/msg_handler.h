#pragma once

#include <string>

class msg_debugger {
    public:
        explicit msg_debugger(int service_id)
            : m_service_id(service_id), m_sent_msg_count(0), m_read_msg_count(0) {}
        virtual ~msg_debugger() {}

        void print_msg(const std::string &msg) const;
        void print_stats() const;

    protected:
        int m_service_id;
        long m_sent_msg_count;
        long m_read_msg_count;

    private:
        void print_prefix() const;
};

class generic_msg_handler : public msg_debugger {
    public:
        explicit generic_msg_handler(int service_id)
            : msg_debugger(service_id) {}
        virtual ~generic_msg_handler() {}

        virtual int send_msg(const std::string &msg) = 0;
        virtual int read_msg(std::string *msg) = 0;
};

class msg_handler : public generic_msg_handler {
    public:
        explicit msg_handler(int service_id)
            : generic_msg_handler(service_id) {}

        int send_msg(const std::string &msg);
        int read_msg(std::string *msg);
};

