#include "polymorphism_virtual.h"

int main() {
    circle c(2);
    ellipse e(5, 1);

    hypotrochoid *h1 = &c;
    hypotrochoid *h2 = &e;

    h1->print_info();
    h2->print_info();

    // if print_info were not virtual, h1->print_info() would call the base implementation instead of the derived one
}
