#pragma once

class hypotrochoid {
    public:
        hypotrochoid(const float r, const float R, const float d)
            : m_r(r), m_R(R), m_d(d) {}

        float m_r, m_R, m_d;
};
class hypocycloid : public hypotrochoid {
    public:
        hypocycloid(const float r, const float R)
            : hypotrochoid(r, R, r) {}
};
class ellipse : public hypotrochoid {
    public:
        ellipse(const float r, const float d)
            : hypotrochoid(r, 2*r, d), m_M(r + d), m_m(r - d) {}

        float m_M, m_m;
};
class circle : public ellipse {
    public:
        circle(const float r)
            : ellipse(r, 0) {}
};
