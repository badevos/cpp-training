#include "inheritance.h"
#include <iostream>

int main() {
    hypotrochoid a(5, 8, 2);
    hypocycloid b(9, 5);
    ellipse c(10, 2);
    circle d(5);

    std::cout << "hypotrochoid a: r=" << a.m_r << " R=" << a.m_R << " d=" << a.m_d << "\n";
    std::cout << "hypocycloid  b: r=" << b.m_r << " R=" << b.m_R << " d=" << b.m_d << "\n";
    std::cout << "ellipse      c: r=" << c.m_r << " R=" << c.m_R << " d=" << c.m_d << " m=" << c.m_m << " M=" << c.m_M << "\n";
    std::cout << "circle       d: r=" << d.m_r << " R=" << d.m_R << " d=" << d.m_d << " m=" << d.m_m << " M=" << d.m_M << "\n";

    return 0;
}
