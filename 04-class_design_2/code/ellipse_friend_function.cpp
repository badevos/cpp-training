#include <iostream>
#include <cmath>

class ellipse {
    private:
        float m_major, m_minor;
    public:
        ellipse(const float major, const float minor) : m_major(major), m_minor(minor) {}
        float area() { return M_PI*(m_major/2)*(m_minor/2); }
        friend ellipse half_clone(const ellipse &);
};

ellipse half_clone(const ellipse &other) {
    return ellipse(other.m_major / 2, other.m_minor / 2);
}

int main() {
    ellipse a(25, 20);
    ellipse b = half_clone(a);
    std::cout << "a.area = " << a.area() << " / b.area = " << b.area() << "\n";
}
