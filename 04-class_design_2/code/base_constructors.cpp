#include <iostream>

class base {
	public:
		base() { std::cout << "base::base()\n"; }
		base(int a, int b) { std::cout << "base::base(a,b)\n"; }
};
class derived : public base {
	public:
		derived() { std::cout << "derived::derived()\n"; }
		derived(int a) : base(a, 0) { std::cout << "derived::derived(a)\n"; }
};
int main() {
    derived d0;
    derived d1(5);
}
