#include <iostream>
class base {
    public:
        virtual ~base() {}
        virtual void print_vars() = 0;
        virtual void print_functions() = 0;
        virtual void print_info() {
            print_vars();
            print_functions();
        }
};
class derived : public base {
    public:
        derived(int a) : a(a) {}
        int a;
        virtual void print_vars() { std::cout << "a=" << a << "\n"; }
        virtual void print_functions() { std::cout << "-" << "\n"; }
};

int main() {
    base *b = new derived(5);
    b->print_info();
    delete b;
}
