#pragma once

#include <string>

class msg_handler;

class msg_debugger {
    public:
        msg_debugger(const msg_handler &handler)
            : m_handler(handler) {}

        void print_msg(const std::string &msg) const;
        void print_stats() const;

    private:
        void print_prefix() const;

        const msg_handler &m_handler;
};

class msg_handler {
    friend class msg_debugger;

    public:
        msg_handler(int service_id)
            : m_service_id(service_id), m_sent_msg_count(0), m_read_msg_count(0), m_dbg(*this) {}

        int send_msg(const std::string &msg);
        int read_msg(std::string *msg);

        const msg_debugger &dbg() const { return m_dbg; }

    private:
        int m_service_id;
        long m_sent_msg_count;
        long m_read_msg_count;
        msg_debugger m_dbg;
};

