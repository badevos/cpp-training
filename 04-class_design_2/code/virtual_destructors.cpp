#include <iostream>
class a {
    public:
        virtual ~a() { std::cout << "~a\n"; }
};
class b : public a {
    public:
        virtual ~b() { std::cout << "~b\n"; }
};
class c : public b {
    public:
        virtual ~c() { std::cout << "~c\n"; }
};
int main() {
    c el;
}
