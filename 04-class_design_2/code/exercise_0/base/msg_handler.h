#pragma once

#include <string>

class msg_handler {
    public:
        msg_handler(int service_id)
            : m_service_id(service_id), m_sent_msg_count(0), m_read_msg_count(0) {}

        void dbg_print_msg(const std::string &msg);
        void dbg_print_stats();

        int send_msg(const std::string &msg);
        int read_msg(std::string *msg);

    private:
        int m_service_id;
        long m_sent_msg_count;
        long m_read_msg_count;
};

