#include "msg_handler.h"

#include <string>
#include <iostream>
#include <cassert>
#include <cstdlib>

static void random_string(std::string *buffer, const int length)
{
    static const std::string sc_bank = "0123456789abcdefghijklmnopqrstuvwxyz";

    buffer->clear();
    for (int i = 0; i < length; i++) {
        buffer->append(1, sc_bank.at(rand() % (sizeof(sc_bank) - 1)));
    }
}

void msg_handler::dbg_print_msg(const std::string &msg)
{
    std::cout << "[" << m_service_id << "] " << msg << "\n";
}

void msg_handler::dbg_print_stats()
{
    std::cout << "[" << m_service_id << "] sent = " << m_sent_msg_count << " / read = " << m_read_msg_count << "\n";
}

int msg_handler::send_msg(const std::string &msg)
{
    dbg_print_msg(">> " + msg);
    //TODO actually send the msg

    m_sent_msg_count++;

    return 0;
}

int msg_handler::read_msg(std::string *msg)
{
    random_string(msg, 16);
    //TODO actually read the msg
    dbg_print_msg("<< " + *msg);

    m_read_msg_count++;

    return 0;
}

int main()
{
    msg_handler h(0);

    h.dbg_print_stats();

    assert(h.send_msg("test0") == 0);

    std::string read_buffer;
    assert(h.read_msg(&read_buffer) == 0);
    assert(h.read_msg(&read_buffer) == 0);
    assert(h.read_msg(&read_buffer) == 0);

    h.dbg_print_stats();

    return 0;
}
