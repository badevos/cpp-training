#include "polymorphism.h"

int main() {
    circle c(2);
    ellipse e(5, 1);

    hypotrochoid *h1 = &c;
    hypotrochoid *h2 = &e;

    h1->set_params(2, 4, 0); // <=> c.set_params(2, 4, 0)
    h2->set_params(4, 8, 1);

    //h1->print_info(); // nope !
    //h2->print_info(); // nope !
}
