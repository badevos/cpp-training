%------------------------------------------------
%       TITLE PAGE
%------------------------------------------------

\title[Introduction]{Class Design II}
\maketitle

%------------------------------------------------

\makeoverview{}

%------------------------------------------------

\section{Classes relationships}
\makecurrentsection{}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Object oriented concepts}
    \begin{itemize}
        \item association
        \begin{itemize}
            \item A knows about B
            \item A has a pointer to a B oject as a member
        \end{itemize}
        \item composition
        \begin{itemize}
            \item A contains a B
            \item A has a B object as a data member
        \end{itemize}
        \item inheritance
        \begin{itemize}
            \item A is a B
            \item A derives from B
        \end{itemize}
    \end{itemize}
\end{frame}

%------------------------------------------------

\section{Friendship}
\makecurrentsection{}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Friend functions}
    \begin{itemize}
        \item gives access to private or protected variables to a non-member function
        \item to use: include a declaration of a function inside a class, prefixed with the \textit{friend} keyword
        \lstinputlisting[basicstyle=\tiny,firstline=4]{code/ellipse_friend_function.cpp}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Friend classes}
    \begin{itemize}
        \item gives access to private or protected variables to another class
        \lstinputlisting[basicstyle=\tiny,firstline=4]{code/ellipse_friend_class.cpp}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Friend classes}
    \begin{itemize}
        \item friendship is \textbf{not} implicitely corresponded
        \begin{itemize}
            \item ellipse is a friend of circle but circle is not a friend of ellipse
        \end{itemize}
        \item friendship is \textbf{not} implicitely transitive
        \begin{itemize}
            \item the friend of a friend is not a friend
        \end{itemize}
        \item friendship is \textbf{not} inherited
        \begin{itemize}
            \item the friend of a base class is not a friend of one of it's derived class
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Exercise 0}
    \begin{itemize}
        \item refactor the msg\_handler class and split all dbg\_* functions into a msg\_debugger class
        \item do \textbf{not} change access specifiers for the msg\_handler class
        \item msg\_handler should provide a dbg() function returning a const reference to the msg\_debugger object
    \end{itemize}
\end{frame}

%------------------------------------------------

\section{Inheritance}
\makecurrentsection{}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Principles}
    \begin{itemize}
        \item extends a \textbf{base} class to a \textbf{derived} class
        \item the \textbf{derived} class inherits members of the \textbf{base} class
        \item multiple classes can inherit from a single one
        \item there can be multiple levels of inheritance
        \item a \textbf{derived} class can inherit from multiple \textbf{base} classes
        \item inheritance is declared in the derived class
        \begin{lstlisting}
    class derived : public base {
        ...
    };
        \end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{\currentname}
    \textbf{Member access}
    \begin{itemize}
        \item a class can be inherited with the 3 access specifiers (public, protected, private)
        \begin{itemize}
            \item restrict access to more accessible members to the level specified
            \item same level or less accessible members keep their access level
        \end{itemize}
        \item for public inheritance:
        \begin{tabular}{ l c c c }
          access & private & protected & public \\
          \hline
          class members         & \textbf{yes} & \textbf{yes} & \textbf{yes} \\
          derived class members & no           & \textbf{yes} & \textbf{yes} \\
          non-members           & no           & no           & \textbf{yes} \\
          \hline
        \end{tabular}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{\currentname}
    \textbf{What do we inherit ?}
    \begin{itemize}
        \item everything but:
        \begin{itemize}
            \item constructors
            \item destructor
            \item assignment operators
            \item friends
            \item private members
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Accessing a base constructor}
    \lstinputlisting[firstline=3]{code/base_constructors.cpp}
\end{frame}

\begin{frame}
    \frametitle{\currentname}
    \textbf{Example inheritance graph}
    \begin{columns}
        \begin{column}{.49\textwidth}
            \begin{itemize}
                \item hypotrochoid (r, R, d)
                \begin{itemize}
                    \item hypocycloid (d = r)
                    \item ellipse (R = 2r)
                    \begin{itemize}
                        \item circle (d = 0)
                    \end{itemize}
                \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{.49\textwidth}
            \includegraphics[width=50mm]{hypotrochoid.png}
        \end{column}
    \end{columns}
    \begin{itemize}
        \item hypocycloid and ellipse are derived from hypotrochoid
        \item hypotrochoid is the base class for hypocycloid and ellipse
        \item circle is derived from ellipse
        \item ellipse is the base class for circle
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Example inheritance graph}
    \lstinputlisting[basicstyle=\tiny,firstline=3]{code/inheritance.h}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Example inheritance graph}
    \lstinputlisting[basicstyle=\tiny,firstline=3]{code/inheritance.cpp}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Multiple inheritance}
    \begin{itemize}
        \item a class can inherit from multiple base classes
        \begin{itemize}
            \item beware of name conflicts
            \item each inherited class can specify a different access level
        \end{itemize}
        \begin{lstlisting}
class b0 { public: void do_stuff(); };
class b1 { public: void do_stuff(); };

class derived : public b0, private b1 {
    public:
        void do_stuff() {
            b0::do_stuff();
            b1::do_stuff();
        }
};
        \end{lstlisting}
        \item try to limit the inheritance chain !
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Multiple inheritance}
    \begin{itemize}
        \item a class can inherit multiple times from the same base class
        \begin{itemize}
            \item you should \textbf{not} do that (and should watch for it)
            \item can lead to the "diamond of death" / "dreaded diamond"
        \end{itemize}
        \begin{lstlisting}
class a { ... };
class b : public a { ... };
class c : public a { ...};
class d : public b, public c { ... };
        \end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Exercise 1}
    \begin{itemize}
        \item refactor exercise 0 by making msg\_handler inherit from msg\_debugger
        \item move service\_id and msg counters from msg\_handler to msg\_debugger
    \end{itemize}
\end{frame}

%------------------------------------------------

\section{Polymorphism}
\makecurrentsection{}

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Type compatibility}
    \begin{itemize}
        \item a pointer to a derived class is type-compatible with a pointer to its base class
        \lstinputlisting[firstline=3]{code/polymorphism_0.cpp}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Type compatibility}
    \begin{itemize}
        \item let's add a method to our base class
        \lstinputlisting[firstline=6,lastline=17]{code/polymorphism.h}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Type compatibility}
    \begin{itemize}
        \item now we have access to it from our derived classes
        \item but pointers to the base class cannot access derived members !
        \lstinputlisting[firstline=3]{code/polymorphism_1.cpp}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Virtual member functions}
    \begin{itemize}
        \item can be redefined in a derived class and can be referenced through a pointer to the base class
        \item declared with the \textit{virtual} keyword (not repeated in the definition)
        \begin{lstlisting}
class hypotrochoid {
    public:
        virtual void print_info() { /* print stuff */ }
};
class ellipse {
    public:
        virtual void print_info() { /* print other stuff */ }
};
        \end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Virtual member functions}
    \begin{itemize}
        \item circle and ellipse are now \textbf{polymorphic} classes
        \lstinputlisting[firstline=3]{code/polymorphism_virtual.cpp}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Abstract classes}
    \begin{itemize}
        \item can only be used as base class
        \item cannot be used to declare objects
        \item have at least one pure virtual function (virtual member functions with no definition)
        \begin{itemize}
            \item definition is replaced by "= 0"
        \end{itemize}
        \begin{lstlisting}
class tracer {
    public:
        virtual void print_info() = 0;
};
class hypotrochoid : public tracer {
    public:
        virtual void print_info() { /* print stuff */ }
};
        \end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Abstract classes}
    \begin{itemize}
        \item can access virtual member functions of the derived class
        \lstinputlisting[basicstyle=\tiny,firstline=2]{code/virtual_member_functions.cpp}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Abstract classes}
    \begin{itemize}
        \item constructors are called in sequence (from first base to most derived)
        \item destructors are called in opposite sequence and should \textbf{always} be virtual !
        \lstinputlisting[basicstyle=\tiny,firstline=2]{code/virtual_destructors.cpp}
        \item calling delete on a base class with a non-virtual destructor is undefined (usually a compiler error)
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Pure abstract classes}
    \begin{itemize}
        \item also knows as \textbf{interfaces}
        \item classes with only pure virtual member functions, no data nor concrete member functions
   \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Exercise 2}
    \begin{itemize}
        \item refactor exercise 1 by making msg\_handler inherit from an interface: generic\_msg\_handler
        \item declare send\_msg and read\_msg pure virtual in generic\_msg\_handler
    \end{itemize}
\end{frame}

%------------------------------------------------

\section{Class design}
\makecurrentsection

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Proper interfaces}
    \begin{itemize}
        \item Interfaces should be \textbf{easy} to use \textit{correctly} and \textbf{hard} to use \textit{incorrectly}
        \item Beware of:
        \begin{itemize}
            \item correctnes % making sure you don't modify data you should not modify
            \item efficiency
            \item encapsulation % provide data and the functions to operate on that data
            \item maintainability
            \item extensibility
            \item conformance to convention % people are not frightened to use your code :p
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Treat class design as type design}
    \begin{itemize}
        \item How should objects be created and destroyed ?
        \item How should object initialization differ from assignment ?
        \item What does it mean for objects to be passed by value ?
        \item What are the restrictions on legal values ?
        \item Does it fit into an inheritance graph ?
        \item What kind of type conversions are allowed ?
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Treat class design as type design}
    \begin{itemize}
        \item What operators and functions make sense ?
        \item What standard functions should be disallowed ?
        \item Who should have access to members ?
        \item What is the undeclared interface ?
        \begin{itemize}
            \item performance
            \item exception safety
            \item resource usage
        \end{itemize}
        \item How general is your type ?
        \item Is a new type really what you need ?
    \end{itemize}
\end{frame}

%------------------------------------------------

\section{References}
\makecurrentsection

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \begin{itemize}
        \item C++ Super-FAQ (isocpp.org/faq)
        \item The C++ Programming Language, Bjarne Stroustrup
        \item Thinking in C++, Bruce Eckel
        \item Effective C++, Scott Meyers
        \item Modern C++:: Generic programming and design patterns applied, Andrei Alexandrescu
    \end{itemize}
\end{frame}

