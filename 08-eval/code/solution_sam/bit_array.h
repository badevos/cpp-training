#pragma once

#include <algorithm>
#include <stdint.h>
#include <iostream>
#include <limits> // numeric_limits

namespace Lib {

    class bit_array {
    public:
        bit_array(unsigned size = 0);
        bit_array(const bit_array&);
        ~bit_array();

        bit_array& operator=(bit_array other);
        friend void swap(bit_array&, bit_array&);
        friend std::ostream& operator<<(std::ostream& os, const bit_array& bits);
        bit_array& operator+=(int nth_bit);
        bit_array& operator-=(int nth_bit);
        bit_array operator++(int);
        bit_array& operator++();
        bit_array operator--(int);
        bit_array& operator--();
        bit_array& operator~();

        bool operator[](int nth_bit) const;

    private:
        bool canRetrieveBitPos(const int nthBit_i) const;
        inline unsigned getDataEntryPos(int nthBit_i) const { return nthBit_i / bitsInDataType; }
        inline unsigned getBitPosInDataEntry(int nthBit_i) const  { return nthBit_i % bitsInDataType; }

        typedef uint32_t sizeType;
        static const unsigned bitsInDataType = std::numeric_limits<sizeType>::digits;
        unsigned m_nbrOfBits;
        unsigned m_nbrDataEntries;
        sizeType* m_data;
    };

} /* namespace Lib */
