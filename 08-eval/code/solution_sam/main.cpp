#include "bit_array.h"

#include <iostream>


int main()
{
    Lib::bit_array a(16);
    std::cout << "a = " << a << "\n";

    a += 2;
    a += 5;
    a += 15;
    a += 17; // should gave an error
    std::cout << "After using +=: a = " << a << "\n";
    a--;
    std::cout << "After using --: a = " << a << "\n";
    a++;
    std::cout << "After using ++: a = " << a << "\n";
    a -= 1;
    a -= 6;
    a -= 9;
    std::cout << "After using -=: a = " << a << "\n";

    Lib::bit_array b(a);
    std::cout << "b = " << b << "\n";

    Lib::bit_array c(16);
    c = b;
    std::cout << "c = " << c << "\n";

    Lib::bit_array d(512);
    ~d;
    d -= 3;
    d -= 511;
    std::cout << "d = " << d << "\n";
}
