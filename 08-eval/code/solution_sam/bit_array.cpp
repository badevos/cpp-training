#include "bit_array.h"

#include <bitset>

namespace Lib
{

/**
 * Instantiate a bit array
 * @param size number of bits to store
 */
bit_array::bit_array(unsigned size)
    : m_nbrOfBits(size)
    , m_nbrDataEntries(size / bitsInDataType + 1)
    , m_data(size ? new sizeType[m_nbrDataEntries]() : 0) // Initialise data array with 0
{
}


bit_array::~bit_array()
{
    delete[] m_data;
}


bit_array::bit_array(const bit_array& other)
    : m_nbrOfBits(other.m_nbrOfBits)
    , m_nbrDataEntries(other.m_nbrDataEntries)
    , m_data(other.m_nbrDataEntries ? new sizeType[m_nbrDataEntries] : 0) // don't Initialise arrays here
{
    std::copy(other.m_data, other.m_data + other.m_nbrDataEntries, m_data);
}


bit_array& bit_array::operator=(bit_array other)
{
    swap(*this, other);
    return *this;
}


void swap(bit_array& lhs, bit_array& rhs)
{
    using std::swap;
    swap(lhs.m_nbrOfBits, rhs.m_nbrOfBits);
    swap(lhs.m_nbrDataEntries, rhs.m_nbrDataEntries);
    swap(lhs.m_data, rhs.m_data);
}


std::ostream& operator<<(std::ostream& os, const bit_array& bits)
{
    os << '{' << bits[bits.m_nbrOfBits-1]; // don't print comma for first element
    for (unsigned pos = bits.m_nbrOfBits-1; pos != 0; --pos) {
        os << ',' << bits[pos-1];
    }
    os << '}';
    return os;
}


/**
 * Checks if the input bit is not outside the range of the bit array. If so, it prints an error.
 * @param nthBit_i  The nth bit to search for
 * @return true or false
 */
bool bit_array::canRetrieveBitPos(int nthBit_i) const
{
    if (nthBit_i < 0 || nthBit_i >= m_nbrOfBits) {
        std::cerr << "Out of limits: "<< nthBit_i << " (range: 0-" << m_nbrOfBits  << ")\n";
        return false;
    }
    return true;
}


/**
 * Sets the nth bit in the container
 * @param nthBit  nth bit in the array
 * @return
 */
bit_array& bit_array::operator+=(int nthBit)
{
    if (!canRetrieveBitPos(nthBit)) return *this;

    unsigned dataPos = getDataEntryPos(nthBit);
    unsigned bitPos = getBitPosInDataEntry(nthBit);
    m_data[dataPos] |= (1 << bitPos);

    return *this;
}


/**
 * Resets the nth bit in the container
 * @param nthBit
 * @return itself
 */
bit_array& bit_array::operator-=(int nthBit)
{
    if (!canRetrieveBitPos(nthBit)) return *this;

    unsigned dataPos = getDataEntryPos(nthBit);
    unsigned bitPos = getBitPosInDataEntry(nthBit);
    m_data[dataPos] |= (1 << bitPos);

    m_data[dataPos] &= ~(1 << bitPos);
    return *this;
}


/**
 * Sets all bits in the container (post-increment)
 * @param unused
 * @return itself as copy
 */
bit_array bit_array::operator++(int)
{
    bit_array tmp(*this);
    operator++();
    return *this;
}

/**
 * Sets all bits in the container (pre-increment)
 * @return itself
 */
bit_array& bit_array::operator++()
{
    std::fill(m_data, m_data + m_nbrDataEntries, std::numeric_limits<sizeType>::max());
    return *this;
}


/**
 * Rests all bits in the container (post-increment)
 * @param unused
 * @return itself as copy
 */
bit_array bit_array::operator--(int)
{
    bit_array tmp(*this);
    operator--();
    return tmp;
}


/**
 * Rests all bits in the container (pre-increment)
 * @return itself
 */
bit_array& bit_array::operator--()
{
    std::fill(m_data, m_data + m_nbrDataEntries, 0);
    return *this;
}


/**
 * Take complement of all bits
 * @return itself
 */
bit_array& bit_array::operator~()
{
    for(unsigned i = 0; i != m_nbrDataEntries; ++i) {
        m_data[i] = ~m_data[i];
    }
    return *this;
}


/**
 * Get nth bit of within the container
 * @param nth_bit the bit to search for
 * @return state of the bit (false/true)
 */
bool bit_array::operator[](int nthBit) const
{
    if (!canRetrieveBitPos(nthBit)) return false;

    unsigned dataPos = getDataEntryPos(nthBit);
    unsigned bitPos = getBitPosInDataEntry(nthBit);

    return (m_data[dataPos] >> bitPos) & 1;
}


} /* namespace Lib */
