#include <iostream>
#include "bit_array.h"

int main() {
    bit_array a(16);
    std::cout << "a = " << a << "\n";
    a += 2; a += 5; a += 15;
    std::cout << "a = " << a << "\n";
    a--;
    std::cout << "a = " << a << "\n";
    a++;
    std::cout << "a = " << a << "\n";
    a -= 1; a -= 6; a -= 9;
    std::cout << "a = " << a << "\n";
    bit_array b(a);
    std::cout << "b = " << b << "\n";
    bit_array c(24);
    c = b;
    std::cout << "c = " << c << "\n";
}

