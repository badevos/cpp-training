#pragma once

#include <stdint.h>

#include <iostream>

class bit_array
{
	private:
		int m_num_bits;
		int m_storage_size;
		uint8_t *m_storage;

	public:
		bit_array(int);
		bit_array(bit_array &);
		~bit_array();

		bit_array &operator=(bit_array);
		int operator[](int) const;
		void operator+=(int);
		void operator-=(int);

		friend void swap(bit_array &a, bit_array &b);
		friend std::ostream &operator<<(std::ostream &, bit_array &);

		bit_array &operator++();
		bit_array operator++(int);
		bit_array &operator--();
		bit_array operator--(int);
		void operator~();
};

