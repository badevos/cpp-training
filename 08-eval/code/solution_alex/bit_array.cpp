#include "bit_array.h"
#include <climits>
#include <algorithm>
#include <string.h>

bit_array::bit_array(int n)
    : m_num_bits(n), m_storage_size(n / CHAR_BIT + 1), m_storage(new uint8_t[m_storage_size]())
{
}

bit_array::bit_array(bit_array &other)
    : m_num_bits(other.m_num_bits), m_storage_size(other.m_storage_size), m_storage(new uint8_t[m_storage_size]())
{
    std::copy(other.m_storage, other.m_storage + m_storage_size, m_storage);
}

bit_array::~bit_array()
{
    delete[] m_storage;
}

bit_array &bit_array::operator=(bit_array other)
{
    swap(*this, other);
    return *this;
}

int bit_array::operator[](int i) const
{
    int p = i / CHAR_BIT;
    if (p < 0 || p >= m_num_bits)
        return -1;

    int q = i % CHAR_BIT;
    return (m_storage[p] >> (CHAR_BIT - q - 1)) & 0x01;
}

void bit_array::operator+=(int i)
{
    int p = i / CHAR_BIT;
    if (p < 0 || p >= m_num_bits)
        return;

    int q = i % CHAR_BIT;
    m_storage[p] |= (1 << (CHAR_BIT - q - 1));
}

void bit_array::operator-=(int i)
{
    int p = i / CHAR_BIT;
    if (p < 0 || p >= m_num_bits)
        return;

    int q = i % CHAR_BIT;
    m_storage[p] &= ~(1 << (CHAR_BIT - q - 1));
}

void swap(bit_array &a, bit_array &b)
{
    using std::swap;
    swap(a.m_num_bits, b.m_num_bits);
    swap(a.m_storage_size, b.m_storage_size);
    swap(a.m_storage, b.m_storage);
}

std::ostream &operator<<(std::ostream &o, bit_array &b)
{
    o << "{ ";
    for (int i = b.m_num_bits; i != 0; i--) {
        o << b[i-1] << " ";
    }
    o << "}";
    return o;
}

bit_array &bit_array::operator++()
{
    std::fill(m_storage, m_storage + m_storage_size, 0xff);
    return *this;
}

bit_array bit_array::operator++(int n)
{
    bit_array b = *this;
    this->operator++();
    return b;
}

bit_array &bit_array::operator--()
{
    std::fill(m_storage, m_storage + m_storage_size, 0);
    return *this;
}

bit_array bit_array::operator--(int n)
{
    bit_array b = *this;
    this->operator--();
    return b;
}

void bit_array::operator~()
{
    for (int i = 0; i < m_storage_size; i++) {
        m_storage[i] = ~m_storage[i];
    }
}

