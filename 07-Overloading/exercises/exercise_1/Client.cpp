#include "Client.h"


namespace XDevices {

Client::Client(DataLink::MacAddress& mac) : Device(mac)
{
}


DataLink::MacAddress Client::getDefaultMacAddress()
{
    DataLink::MacAddress defaultAddr(0x1E, 0, 0, 0, 0, 0x01);
    return defaultAddr;
}


}
