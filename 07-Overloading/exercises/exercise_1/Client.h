#pragma once

#include "Device.h"
#include "MacAddress.h"

namespace XDevices {

class Client : public Device {
public:
    Client(DataLink::MacAddress& sourceMac);

    static DataLink::MacAddress getDefaultMacAddress();
};

}
