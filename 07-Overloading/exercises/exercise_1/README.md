Operator Overloading & Swap idiom
---------------------------------

1. Swap idiom for MacAddress & Device:
   - Implement the correct swap methods & assignment operators
   - Make sure the Server cannot be copied (make copy c'tor & operator= private). Compilation should fail on main.cpp:25.
   - Test by creating client2 with a different MAC as the default, copying client2 in client1. See output of both MACs.

2. Multiple clients with incrementing MAC addresses:
   - Store the default MAC into the Client, let it increase when a Client is created (default c'tor)
     e.g. increasing 1E:00:00:FF:FF becomes 1E:00:01:00:00

   The following are not mandatory:
   - [bonus] Create 1000 Clients, verify by printing the last Client's MAC address
   - [bonus] Add all clients to a map with a creation id as key. Request the MAC address of a certain Client using std::cin.
