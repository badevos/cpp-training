#include "MacAddress.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>

namespace DataLink {


MacAddress::MacAddress(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3, uint8_t b4,  uint8_t b5)
    : mac(new uint8_t[macAddressLength_c])
{
    mac[0] = b0;
    mac[1] = b1;
    mac[2] = b2;
    mac[3] = b3;
    mac[4] = b4;
    mac[5] = b5;
}


MacAddress::~MacAddress() {
    delete[] mac;
}


MacAddress::MacAddress(const MacAddress& other)
    : mac(new uint8_t[macAddressLength_c])
{
    std::cout << "A copy of a MAC address was made" << '\n';
    std::copy(other.mac, other.mac + macAddressLength_c, mac);
}


std::string MacAddress::toString() const
{
    std::ostringstream ss;
    ss << std::uppercase << std::setfill('0') << std::hex << std::setw(2) << int(mac[0]);
    for(unsigned byte = 1; byte < macAddressLength_c; ++byte) {
        ss << ':' << std::setw(2) << int(mac[byte]);
    }
    return ss.str();
}

}

