#pragma once

#include "Device.h"

namespace DataLink { // The only way to forward declare a type from another namespace
class MacAddress; // Not needed as retrieved from Device.h but good habbit to include all used types
}

namespace XDevices {


class Server : public Device {
public:
    Server(DataLink::MacAddress& soureMac);
    virtual ~Server();

};

}
