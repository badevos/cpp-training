#include "Device.h"

#include "MacAddress.h"

namespace XDevices {

unsigned Device::m_numberOfDevices = 0;

Device::Device(DataLink::MacAddress& macAddress)
    : m_sourceMacAddress(macAddress)
{
    ++m_numberOfDevices;
}


Device::~Device()
{
    --m_numberOfDevices;
}


const DataLink::MacAddress& Device::getMacAddress() const
{
    return m_sourceMacAddress;
}

unsigned Device::getNumberOfDevices()
{
    return m_numberOfDevices;
}

}
