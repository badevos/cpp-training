#include "MacAddress.h"
#include "Client.h"
#include "Server.h"

#include <iostream>


int main()
{
    DataLink::MacAddress serverMacAddress(0x01, 0, 0, 0, 0, 0xF0);
    std::cout << "MAC address of the server = " << serverMacAddress.toString() << '\n';

    XDevices::Server server(serverMacAddress);

    DataLink::MacAddress defaultMacAddress = XDevices::Client::getDefaultMacAddress();
    XDevices::Client client1(defaultMacAddress);
    std::cout << "Default MAC address for a client = " << defaultMacAddress.toString() << '\n';

    std::cout << "Client1 MAC address " << client1.getMacAddress().toString() << ", should be equal to the default."<< '\n';

#if MOVE_LINE_DOWN_STEP_BY_STEP

    // Solution 1

    XDevices::Server server2(server); // Should fail to compile

    DataLink::MacAddress client2mac(0xE1, 0, 0, 0, 0, 0xA2);
    XDevices::Client client2(client2mac);
    std::cout << "Client2 MAC address " << client2.getMacAddress().toString() << '\n';

    client1 = client2;
    std::cout << "Client1 MAC address " << client1.getMacAddress().toString() << '\n';


    // Solution 2

    XDevices::Client client3;
    std::cout << "Client3 got generated MAC address " << client3.getMacAddress().toString() << '\n';
    XDevices::Client client4;
    std::cout << "Client4 got generated MAC address " << client4.getMacAddress().toString() << '\n';

    std::cout << "The number of devices is " << XDevices::Device::getNumberOfDevices() << '\n';
#endif

    return 0;
}

