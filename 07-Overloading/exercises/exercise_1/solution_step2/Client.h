#pragma once

#include "Device.h"
#include "MacAddress.h"

namespace XDevices {

class Client : public Device {
public:
    Client();
    virtual ~Client();
    Client(DataLink::MacAddress& sourceMac);
    Client(const Client& other);

    static const DataLink::MacAddress& getDefaultMacAddress();

private:
    static DataLink::MacAddress m_defaultAddr;
};

}
