#include "Client.h"


namespace XDevices {

DataLink::MacAddress Client::m_defaultAddr(0xE1, 0xFF, 0x22, 0xFE, 0xFF, 0xFE);


Client::Client() : Device(m_defaultAddr)
{
    ++m_defaultAddr;
}



Client::Client(DataLink::MacAddress& mac) : Device(mac)
{
}


Client::Client(const Client& other) : Device(other)
{
    //++m_sourceMacAddress; // You could also increment the MAC address on copy
}


Client::~Client()
{
}

const DataLink::MacAddress& Client::getDefaultMacAddress()
{
    return m_defaultAddr;
}


}
