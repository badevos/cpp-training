#include "Device.h"

#include "MacAddress.h"

namespace XDevices {

unsigned Device::m_numberOfDevices = 0;

Device::Device(DataLink::MacAddress& macAddress)
    : m_sourceMacAddress(macAddress)
{
    ++m_numberOfDevices;
}


Device::~Device()
{
    --m_numberOfDevices;
}


Device::Device(const Device& other)
    : m_sourceMacAddress(other.m_sourceMacAddress)
{
    ++m_numberOfDevices;
}


Device& Device::operator=(Device other)
{
    swap(*this, other);
    return *this;
}


void swap(Device lhs, Device rhs)
{
    using std::swap;
    swap(lhs.m_sourceMacAddress, rhs.m_sourceMacAddress);
    // no swapping of static members, there is only 1 instance
}


const DataLink::MacAddress& Device::getMacAddress() const
{
    return m_sourceMacAddress;
}


unsigned Device::getNumberOfDevices()
{
    return m_numberOfDevices;
}

}
