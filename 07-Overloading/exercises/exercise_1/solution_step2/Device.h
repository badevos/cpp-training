#pragma once

namespace DataLink {
class MacAddress;
}

namespace XDevices {

class Device {
public:
    virtual ~Device();
    Device(DataLink::MacAddress& srcMac);
    Device(const Device& other);
    Device& operator=(Device other);
    friend void swap(Device lhs, Device rhs);

    const DataLink::MacAddress& getMacAddress() const;

    static unsigned getNumberOfDevices();

protected:
    DataLink::MacAddress& m_sourceMacAddress;

private:
    static unsigned m_numberOfDevices;
};

}
