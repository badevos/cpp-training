#include "MacAddress.h"
#include "Client.h"
#include "Server.h"

#include <iostream>
#include <vector>

const unsigned c_numberOfClients = 1000;

int main()
{
    DataLink::MacAddress serverMacAddress(0x01, 0, 0, 0, 0, 0xF0);
    std::cout << "MAC address of the server = " << serverMacAddress.toString() << '\n';

    XDevices::Server server(serverMacAddress);

    DataLink::MacAddress defaultMacAddress = XDevices::Client::getDefaultMacAddress();
    XDevices::Client client1(defaultMacAddress);
    std::cout << "Default MAC address for a client = " << defaultMacAddress.toString() << '\n';

    std::cout << "Client1 MAC address " << client1.getMacAddress().toString() << ", should be equal to the default."<< '\n';


    // Solution 1

    ///XDevices::Server server2(server); // Should fail to compile

    DataLink::MacAddress client2mac(0xE1, 0, 0, 0, 0, 0xA2);
    XDevices::Client client2(client2mac);
    std::cout << "Client2 MAC address " << client2.getMacAddress().toString() << '\n';

    client1 = client2;
    std::cout << "Client1 MAC address " << client1.getMacAddress().toString() << '\n';


    // Solution 2

    XDevices::Client client3;
    std::cout << "Client3 got generated MAC address " << client3.getMacAddress().toString() << '\n';
    XDevices::Client client4;
    std::cout << "Client4 got generated MAC address " << client4.getMacAddress().toString() << '\n';

    std::cout << "The number of devices is " << XDevices::Device::getNumberOfDevices() << '\n';

    std::vector<XDevices::Device*> clients;
    for(unsigned c = 0; c < c_numberOfClients; ++c) {
        XDevices::Client* client = new XDevices::Client;
        clients.push_back(client);
    }

    unsigned id;
    std::cout << "Give me the client ID from 1 to 1000: ";
    std::cin >> id;
    std::cout << '\n';

    if (id < 1 || id > c_numberOfClients) {
        std::cout << "Wrong input, the id must lay between 1 and " << c_numberOfClients << '\n';
    } else {
        std::cout << "This is its MAC address: " << clients[id-1]->getMacAddress().toString() << '\n';
    }

    for(unsigned c = 0; c < c_numberOfClients; ++c) {
        delete clients[c];
    }
    clients.clear();

    return 0;
}

