#pragma once

#include "AFile.h"

#include <string>

namespace FileSystem
{

class File : public AFile {
public:
    File(const std::string &name_, const std::string &content_);

    virtual std::string get_name() const { return name; }
    virtual bool is_folder() const { return false; }
    virtual std::size_t get_size() const;

private:
    std::string content;

};


}
