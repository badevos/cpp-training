#pragma once

#include <string>
#include <cstddef>

namespace FileSystem
{

class AFile {
public:
    AFile(const std::string& name_) : name(name_) {};
    virtual ~AFile() {}
    virtual std::string get_name() const = 0;
    virtual bool is_folder() const = 0;
    virtual std::size_t get_size() const { return name.size(); }

protected:
    const std::string name;
};

} // FileSystem
