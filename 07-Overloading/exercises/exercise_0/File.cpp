#include "File.h"

namespace FileSystem
{

File::File(const std::string &name_, const std::string &content_)
    : AFile(name_)
    , content(content_)
{
}


std::size_t File::get_size() const
{
    return AFile::get_size() + content.size();  // also add size of the base class (= its name)
}



}
