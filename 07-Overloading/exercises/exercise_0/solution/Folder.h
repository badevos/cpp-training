#pragma once

#include "AFile.h"
#include <vector>

namespace FileSystem
{

class File;

class Folder : public AFile {
public:
    Folder(const std::string &dir_name_);
    void print_dir() const;
    void add(AFile &file_);

    virtual bool is_folder() const { return true; }
    virtual std::size_t get_size() const;
    virtual std::string get_name() const { return '/'+name; }

    friend std::ostream& operator<<(std::ostream& os, const Folder& folder);

private:
    typedef std::vector<AFile *> Files;
    typedef std::vector<AFile *>::const_iterator itFiles;
    Files fileList;
};


} // namespace FileSystem
