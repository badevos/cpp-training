#include "File.h"
#include "Folder.h"

#include <iostream>

using namespace FileSystem;

int main () {

    // Create some files
    File file1("firstFileName", "File1 content");
    std::cout << "Size of file1: " << file1.get_size() << '\n';

    File file2("SecondFileName", "0123456789");
    std::cout << "Size of file2: " << file2.get_size() << '\n';

    // Create a folder
    Folder parentDir("dir1");
    std::cout << parentDir.get_name() << '\n';
    parentDir.add(file1);

    // Create a folder inside parent folder
    Folder childDir("dir2");
    std::cout << childDir.get_name() << '\n';
    parentDir.add(childDir);

    childDir.add(file2);

    std::cout << parentDir << '\n';

    std::cout << "Size of dir1: " << childDir.get_size() << '\n';

}

