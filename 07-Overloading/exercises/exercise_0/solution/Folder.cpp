#include "Folder.h"
#include "File.h"

#include <iostream>

namespace FileSystem
{

Folder::Folder(const std::string &dir_name_) : AFile(dir_name_)
{
}


void Folder::print_dir() const
{
    std::cout << *this;
}


void Folder::add(AFile &file_)
{
    fileList.push_back(&file_);
}


std::size_t Folder::get_size() const
{
    std::size_t total_size = AFile::get_size(); // also add size of the base class (= its name)
    for (itFiles it = fileList.begin(); it != fileList.end(); ++it) {
        AFile* file = *it;
        total_size += file->get_size();
    }
    return total_size;
}


std::ostream& operator<<(std::ostream& os, const Folder& folder)
{
    os << "Contents of dir " << folder.name << ": ";
    for (Folder::itFiles it = folder.fileList.begin(); it != folder.fileList.end(); ++it) {
        AFile* file = *it;
        os << file->get_name() << ' ';
    }
    os << '\n';
}

} // namespace FileSystem
