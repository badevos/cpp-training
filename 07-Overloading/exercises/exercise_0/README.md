Operator overloading
--------------------

Extend the file system exercise from session 6.

Add the necessary operators to the file system implementation:
- To print the files within a folder (reuse existing print_dir())
- To add/remove a file from a folder (-/+/-=/+=)
- Define the unary increment/decrement operators as private to avoid confusion (no body needed)
- Implement function object within the Folder class (subclass) which sorts the files within the Folder.
- [bonus] To compare files based on their size and md5 number, a random number generated on file creation (==/!=/</>/<=/>=)
