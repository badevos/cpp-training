#include <iostream>

struct Test
{
    Test() {} // override
    Test(const Test &t) {
        std::cout << "Copy constructor" << '\n';
    }
    Test& operator= (const Test &t) {
        std::cout << "Assignment operator=" << '\n';
        return *this;
    }
};

int main()
{
    Test t1, t2;
    t2 = t1; // assignment operator
    Test t3 = t1; // copy constructor
    Test t4(t1); // copy constructor
}

