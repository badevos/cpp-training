int print(int a, int b) {};
// double print(int a, int b); //does not compile
double print(int a, unsigned b) {}; // compiles and NOT ambiquous, different primitive type

struct Class {
  void func(int value) {};
  void func(int value) const {};

  void func(int& value) {}; // compiles but is ambiguous once called
};

int main()
{
  print(1, 2);
  int a;
  Class A;
  // A.func(a); // ambiguous
}
