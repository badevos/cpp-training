#include <iostream>

struct Base {
    void print() { print(1); }
    virtual void print(int a) { std::cout << "Base: " << a << "\n"; }
};

struct A : public Base {
    using Base::print;
    void print() {
        print(2);
    }
    virtual void print(int a) const { std::cout << "A: " << a << "\n"; }
};

int main() {
    Base * obj = new A;
    obj->print(2);

    delete obj;
    return 0;
}
