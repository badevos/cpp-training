#include <iostream>

struct Base {
    virtual void print() {
        std::cout << "Base" << '\n';
    }
};

struct A : public Base {
    virtual void print() {
        std::cout << "A" << '\n';
    }
};

int main() {
    Base * obj = new A;
    obj->print();

    delete obj;
    return 0;
}

