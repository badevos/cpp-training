#include <iostream>

class String {
public:
  String(const char*);
  ~String();
  String(const String&);
  String& operator=(const String& other);
  friend std::ostream& operator<<(std::ostream& os, const String& string);

private:
  unsigned size;
  char* txt;
};
