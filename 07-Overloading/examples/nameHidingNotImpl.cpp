#include <iostream>

struct Base {
    virtual void print() { std::cout << "Base" << "\n"; }
    virtual void print(int a) { std::cout << a << "\n"; }
};

struct A : public Base {
    virtual void print() { Base::print(); }
    // hides Base::print(int a)a
};

int main() {
    A obj;
    obj.print(2); // error! no matching function call
    return 0;
}
