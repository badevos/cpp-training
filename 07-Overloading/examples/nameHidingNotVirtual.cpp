#include <iostream>

struct Base {
    void print() {
        std::cout << "Base" << '\n';
    }
    int member;
};

struct A : public Base {
    void print() { // hides Base::print()
        Base::print();
    }
    int member; // hides Base::member
};

int main() {
    A obj;
    obj.print();
    return 0;
}
