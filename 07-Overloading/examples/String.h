#include <iostream>

class String {
public:
  String(const char*);
  ~String();
  String(const String&);

  String& operator=(String other);
  friend void swap(String& lhs, String& rhs);
  friend std::ostream& operator<<(std::ostream& os, const String& string);

private:
  unsigned size;
  char* txt;
};
