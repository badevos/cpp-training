#include <iostream>

struct Base {
    void f(double x) { std::cout << x << '\n'; }  // Doesn't matter whether or not this is virtual
};

struct A : public Base {
    void f(char c) { std::cout << c << '\n'; }  // Doesn't matter whether or not this is virtual
};

int main()
{
    A* a = new A();
    Base* b = a;
    b->f(65.3); // OK: passes 65.3 to f(double x)
    a->f(65.3); // NOK: converts 65.3 to a char by passing it to f(char c) instead

    delete a;
    return 0;
}
