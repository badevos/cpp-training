#include "String.h"
#include <algorithm>

// Constructor
String::String(const char* text)
  : size(sizeof(text)/sizeof(char))
  , txt(size ? new char[size] : 0)
{
  std::copy(text, text+size, txt);
}

// Destructor
String::~String()
{
  delete[] txt;
}

// Copy constructor
String::String(const String& other)
  : size(other.size)
  , txt(size ? new char[other.size] : 0)
{
  std::copy(other.txt, other.txt + size, txt);
}

// Assignment operator
String& String::operator=(String other) // by value
{
  swap(*this, other);
  return *this;
}

// Swap function
void swap(String& lhs, String& rhs)
{
  using std::swap;
  swap(lhs.size, rhs.size);
  swap(lhs.txt, rhs.txt);
}

// Stream operator
std::ostream& operator<<(std::ostream& os, const String& string)
{
  os << string.txt;
  return os;
}


int main() {

  String a("stringA");
  std::cout << "This should be a: " << a << '\n';

  String b("stringB");
  std::cout << "This should be b: " << b << '\n';

  String c(b);
  std::cout << "C should be equal to b: " << c << '\n';

  a = b;
  std::cout << "A should be equal to b now: " << a << '\n';
}
