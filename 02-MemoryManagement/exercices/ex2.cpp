#include <new>

struct S1
{
    int  m_a;
    char m_b;
};

struct S2
{
    int  m_a;
    char m_b;
    S2(int a, char b) : m_a(a), m_b(b) {}
};

int main()
{
    // Dynamically allocate a S2 with its members initialized to 0


    // Dynamically allocate a S1 with its members initialized to 0


    // Define an automatic (stack) S2 variable with its members initialized to 0


    // Define an automatic (stack) S1 variable with its members initialized to 0


    // Allocate an array of 100 S2


    // Allocate an array of 100 S1 with their members initialized to 0


    //Free it


    // Use placement new to create an S2 on a preallocated buffer


    // Free it


}

