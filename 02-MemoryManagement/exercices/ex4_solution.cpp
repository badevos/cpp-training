#include <iostream>

struct Mutex {

    void lock() {
        std::cout << "mutex locked" << std::endl;
    }

    void unlock() {
        std::cout << "mutex unlocked" << std::endl;
    }

};


struct Lock{
    Lock(Mutex* mutex, bool lock = true) : m_mutex(mutex), m_locked(false){
        if (lock) {
            this->lock();
        }
    }

    void lock () {
        m_mutex->lock();
        m_locked = true;
    }

    void unlock () {
        m_mutex->unlock();
        m_locked = false;
    }

    ~Lock() {
      if (m_locked) {
          m_mutex->unlock();
      }
    }

    bool m_locked;
    Mutex* m_mutex;
};

Mutex mutex;


void CriticalSection1()
{
    Lock lock(&mutex);
    std::cout << "enter critical section 1" << std::endl;

    std::cout << "process critical section 1" << std::endl;

    std::cout << "exit critical section 1" << std::endl;
}

void CriticalSection2()
{
    Lock lock(&mutex);
    std::cout << "enter critical section 2" << std::endl;

    std::cout << "process critical section 2" << std::endl;

    std::cout << "exit critical section 2" << std::endl;
}

int main () {

    CriticalSection1();

    std::cout << "Non critical section" << std::endl;

    CriticalSection2();
}
