#include <iostream>

struct Mutex {

    void lock() {
        std::cout << "mutex locked" << std::endl;
    }

    void unlock() {
        std::cout << "mutex unlocked" << std::endl;
    }

};

struct Lock{

    // Your implementation here

};

Mutex mutex;


void CriticalSection1()
{
    std::cout << "enter critical section 1" << std::endl;

    std::cout << "process critical section 1" << std::endl;

    std::cout << "exit critical section 1" << std::endl;
}

void CriticalSection2()
{
    std::cout << "enter critical section 2" << std::endl;

    std::cout << "process critical section 2" << std::endl;

    std::cout << "exit critical section 2" << std::endl;
}

int main () {

    CriticalSection1();

    std::cout << "Non critical section" << std::endl;

    CriticalSection2();
}
