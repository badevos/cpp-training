#include <iostream>


struct Test
{
    Test(unsigned int n) : m_array(new int[n]()) {}

    ~Test() {
        delete[] m_array;
    }

    int* m_array;
};


int main()
{
    unsigned int size = 50;

    Test t(size);

    for (int i=0; i < size; i++) {
        std::cout << t.m_array[i] << std::endl;
    }
}
