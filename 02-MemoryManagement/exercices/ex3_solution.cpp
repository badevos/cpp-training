#include <string>
#include <memory>
#include <iostream>

struct String {
    String(const char * s) : m_string(s) {}
    ~String() {std::cout << "String destructor called" << std::endl;}

    std::string m_string;
};


std::auto_ptr<String> getString(const char* str) {
    String *s = new String(str);
    return std::auto_ptr<String>(s);
}

void printandDestroyString(std::auto_ptr<String> s) {
    std::cout << s->m_string << std::endl;
}


int main() {

    std::auto_ptr<String> ap = getString("Hello");
    printandDestroyString(ap);
#if 0
    ap->m_string = "test";
#endif
}



