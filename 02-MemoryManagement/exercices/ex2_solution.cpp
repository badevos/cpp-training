#include <new>

struct S1
{
    int  m_a;
    char m_b;
};

struct S2
{
    int  m_a;
    char m_b;
    S2(int a, char b) : m_a(a), m_b(b) {}
};

int main()
{
    // Dynamically allocate a S2 with its members initialized to 0
    S2* x1 = new S2(0,0);

    // Dynamically allocate a S1 with its members initialized to 0
    S1* x2 = new S1();

    // Define an automatic (stack) S2 variable with its members initialized to 0
    S2 x3(0,0);

    // Define an automatic (stack) S1 variable with its members initialized to 0
    S1 x4 = {0,0};

    // Allocate an array of 100 S2
//    S2* a1 = new S2[100];



    // Allocate an array of 100 S1 with their members initialized to 0
    S1* a2 = new S1[100]();

    // Use placement new to create an S2 on a preallocated buffer
    char* buffer = new char[sizeof(S1)];
    S1* y = new (buffer) S1;

    // Free it
    y->~S1();
    delete buffer;

}

