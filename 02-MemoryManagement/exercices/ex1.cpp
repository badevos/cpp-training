#include <iostream>


struct Test
{
 // your implementation here
};


int main()
{
    unsigned int size = 50;

    Test t(size);

    for (int i=0; i < size; i++) {
        std::cout << t.m_array[i] << std::endl;
    }
}
