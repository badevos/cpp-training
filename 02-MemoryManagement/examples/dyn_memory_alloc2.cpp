#include <iostream>
#include <cstring>

struct String {
    char* txt;  // array == pointer!
    String() : txt(new char[32]) {}
    ~String() { delete [] txt; }
};

int main()
{
    String string1, string2;

    strcpy(string1.txt,"string1");

    std::cout << "string1: " << string1.txt << std::endl;

    string2 = string1;
    std::cout << "copied string1 to string2" << std::endl;

    strcpy(string2.txt,"string2");

    std::cout << "string1: " << string1.txt << std::endl;
    std::cout << "string2: " << string2.txt << std::endl;

    return 0;
}

