#include <iostream>

void init() {
    // Put rubbish into memory

    int* dummy = new int[2048];
    std::fill_n(dummy, 2048, 0xAA55AA55);
    delete[] dummy;
}


struct SimpleStruct {
    int a, b;
    char c;
};


struct ConstructorStruct {
    int a, b;
    char c;
    ConstructorStruct(){
        c = 'x';
    };
};


int main()
{

    init();

    //Arrays

    std::cout << "Arrays" << std::endl << "----------------------------" << std::endl;

    int x[10];

    int* y = new int[10];
    int* z = new int[10]();

    for(int i=0; i<10; i++) {
        std::cout << "x[ "  << i << "]: " << x[i] << std::endl;
    }
    for(int i=0; i<10; i++) {
        std::cout << "y[ "  << i << "]: " << y[i] << std::endl;
    }
    for(int i=0; i<10; i++) {
        std::cout << "z[ "  << i << "]: " << z[i] << std::endl;
    }

    // Objects

    std::cout << "Objects: SimpleStruct" << std::endl << "----------------------------" << std::endl;

    SimpleStruct s1;
    SimpleStruct* s2 = new SimpleStruct;
    SimpleStruct* s3 = new SimpleStruct();

    std::cout << "s1: a = "  << s1.a << ", b = " << s1.b << ", c = " << s1.c << std::endl;
    std::cout << "s2: a = "  << s2->a << ", b = " << s2->b << ", c = " << s2->c << std::endl;
    std::cout << "s3: a = "  << s3->a << ", b = " << s3->b << ", c = " << s3->c << std::endl;

    std::cout << "Objects: ConstructorStruct" << std::endl << "----------------------------" << std::endl;

    ConstructorStruct s4;
    ConstructorStruct* s5 = new ConstructorStruct;
    ConstructorStruct* s6 = new ConstructorStruct();

    std::cout << "s4: a = "  << s4.a << ", b = " << s4.b << ", c = " << s4.c << std::endl;
    std::cout << "s5: a = "  << s5->a << ", b = " << s5->b << ", c = " << s5->c << std::endl;
    std::cout << "s6: a = "  << s6->a << ", b = " << s6->b << ", c = " << s6->c << std::endl;


}
