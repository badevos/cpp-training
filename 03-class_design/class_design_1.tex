%------------------------------------------------

\title[Introduction]{Class Design I}
\maketitle

%------------------------------------------------

\makeoverview

%------------------------------------------------

\section{Access specifiers}
\makecurrentsection

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \begin{itemize}
        \item private
        \begin{itemize}
            \item accessible only from within other members of the same class/struct/union (or their \textit{friends})
        \end{itemize}
        \item protected
        \begin{itemize}
            \item accessible from other members of the same class/struct/union (or their \textit{friends}), and members of derived classes
        \end{itemize}
        \item public
        \begin{itemize}
            \item accessible from anywhere where the object is visible
        \end{itemize}
    \end{itemize}
\end{frame}

%------------------------------------------------

\section{Structures}
\makecurrentsection

%------------------------------------------------

\begin{frame}
    \frametitle{\currentname}
    \textbf{C++ data structures}
    \begin{itemize}
        \item User defined data type used to combine data items of different kinds
        \item Usually used to represent a record
        \item Ellipse example:
        \begin{itemize}
            \item major-axis length
            \item minor-axis length
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Defining a structure}
    \begin{itemize}
        \begin{lstlisting}
            struct [tag] {
                access_specifier_0:
                    member0;
                access_specifier_1:
                    member1;
            } [vars];
        \end{lstlisting}
        \item Access defaults to \textbf{public} (more on that in the \textbf{class} slides)
        \item Member definition is any valid C++ variable definition
    \end{itemize}
    \begin{figure}
        \centering
        \begin{tabular}{c}
            \lstinputlisting{code/ellipse_struct.hpp}
        \end{tabular}
    \end{figure}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Accessing structure members}
    \begin{itemize}
        \item Access any member with the \textbf{member access operator}
        \begin{lstlisting}
            an_ellipse.major = 15;
            an_ellipse.minor = 10;
        \end{lstlisting}
        \item For pointers to struct, access via the \textbf{arrow operator}
        \begin{lstlisting}
            another_ellipse->major = 20;
            another_ellipse->minor = 10;
        \end{lstlisting}
        \item You can use the assignment operator to copy its members
        \begin{lstlisting}
            an_ellipse = *another_ellipse;
        \end{lstlisting}
    \end{itemize}
\end{frame}

%------------------------------------------------
\section{Classes}
\makecurrentsection

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Classes}
    \begin{itemize}
        \item Expanded concept of data structure
        \begin{itemize}
            \item contains data \textbf{and} functions as members
        \end{itemize}
        \item An \textbf{object} is an instantiation of a class
        \begin{lstlisting}
            class [name] {
                access_specifier_0:
                    member0;
                access_specifier_1:
                    member1;
                ...
            } [objects];
        \end{lstlisting}
        \item Access defaults to \textbf{private}
        \begin{itemize}
            \item \textbf{struct} defaults to \textbf{public}
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Class declaration}
    \begin{columns}[onlytextwidth, t]
        \begin{column}{.5\textwidth}
            \begin{itemize}
                \item The \textbf{ellipse} class:
                \lstinputlisting{code/ellipse_class_0.hpp}
            \end{itemize}
        \end{column}
        \begin{column}{.5\textwidth}
            \begin{itemize}
                \item equivalent to:
                \lstinputlisting{code/ellipse_class_0_struct_eq.hpp}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Accessing class members}
    \begin{itemize}
        \item Access any public member of an object with the \textbf{member access operator}
        \begin{lstlisting}
std::cout << "Area = " << an_ellipse.area() << "\n";
        \end{lstlisting}
    \item For pointers to a class object, access via the \textbf{arrow operator}
        \begin{lstlisting}
std::cout << "Area = " << another_ellipse->area() << "\n";
        \end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Constructors}
    \begin{definition}
        A \textbf{constructor} is a special non-static member function of a class that is used to initialize objects of its class type.
    \end{definition}
    \begin{itemize}
        \item \textit{Automatically} called whenever a new object is created
        \item Declared like a regular member function, name matches the class name
        \item Cannot be called explicitely, and called only once
        \item Neither the constructor prototype declaration nor the definition have return values
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Constructors}
    \begin{columns}[onlytextwidth, t]
        \begin{column}{.5\textwidth}
            \begin{itemize}
                \item Header file:
                \begin{lstlisting}[basicstyle=\footnotesize]
class ellipse {
  public:
    ellipse(int maj, int min);
    ...
                \end{lstlisting}
            \end{itemize}
        \end{column}
        \begin{column}{.5\textwidth}
            \begin{itemize}
                \item Source file:
                \begin{lstlisting}[basicstyle=\footnotesize]
ellipse::ellipse(int maj, int min)
{
  // init code
  ...
}
                \end{lstlisting}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Sidenote on code organization}
    \begin{itemize}
        \item \lstinline{#include} \textbf{copies} everything in place which can create huge compilation units
        \item everything which is public (as in public API) should go in a header
        \item everything which is private (implementation details) should go into the source
        \item \textbf{very} simple methods (as in one lines like getters or setters) should be inlined in the header
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Sidenote on code organization (header file)}
    \lstinputlisting{code/ellipse_class_1.hpp}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Sidenote on code organization (source file)}
    \lstinputlisting[firstline=4]{code/ellipse_class_1.cpp}
    \begin{itemize}
        \item Notice the use of the scope operator (::) used here to define a member of a class outside the class itself
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Exercise 0}
    \begin{itemize}
        \item Design and implement the 'point' class
        \item It must have:
        \begin{itemize}
            \item a constructor which takes two floats (the two coordinates of the point)
            \item a move() function which takes two floats and moves the coordinates by those two arguments
            \item accessors to set/get the coordinates
        \end{itemize}
        \item Write declaration in a header file, and definition in a source file
        \item Have a main function which quicky tests the behaviour of your class
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Types of constructors}
    \begin{itemize}
        \item default constructor
        \begin{itemize}
            \item called without any argument
        \end{itemize}
        \item parameterized constructor
        \begin{itemize}
            \item called with one (with \textit{explicit} keyword) or multiple arguments
        \end{itemize}
        \item converting constructor
        \begin{itemize}
            \item called with one argument (without \textit{explicit} keyword)
        \end{itemize}
        \item copy constructor
        \begin{itemize}
            \item called with another object of the same type
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Overloading constructors}
    \begin{itemize}
        \item Constructors can be overloaded with a different number of parameters and/or parameters of different types
    \end{itemize}
    \begin{columns}[onlytextwidth, t]
        \begin{column}{.5\textwidth}
            \begin{itemize}
                \item Definition:
                \begin{lstlisting}[basicstyle=\footnotesize]
class ellipse {
  public:
    ellipse(int maj, int min);
    ellipse();
    ...
                \end{lstlisting}
            \end{itemize}
        \end{column}
        \begin{column}{.5\textwidth}
            \begin{itemize}
                \item Usage:
                \begin{lstlisting}[basicstyle=\footnotesize]
ellipse a(5, 10);
ellipse b;
                \end{lstlisting}
            \end{itemize}
        \end{column}
    \end{columns}
    \begin{itemize}
        \item Calling the default constructor requires \textbf{no} parentheses:
        \begin{lstlisting}[basicstyle=\footnotesize]
ellipse a;   // creates an object a of type ellipse
ellipse b(); // declares a function b which returns an object of type ellipse
        \end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Member initializer lists}
    \begin{itemize}
        \item Constructors can initialize members directly without statements
        \begin{itemize}
            \item Can be used in a header file:
            \begin{lstlisting}[basicstyle=\footnotesize]
class ellipse {
    public:
        ellipse(int major, int minor)
            : m_major(major), m_minor(minor) {}
        ...
            \end{lstlisting}
            \item or in a source file:
            \begin{lstlisting}[basicstyle=\footnotesize]
ellipse::ellipse(int major, int minor)
    : m_major(major), m_minor(minor)
{
}
            \end{lstlisting}
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Member initializer lists}
    \begin{itemize}
        \item For fundamental types (void + arithmetic types):
        \begin{itemize}
            \item same as statement init
        \end{itemize}
        \item For objects :
        \begin{itemize}
            \item if not used default constructor is called
            \item generates an error if no default constructor is available
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Destructors}
    \begin{definition}
        A \textbf{destructor} is a special non-static member function that is called when the lifetime of an object ends.
    \end{definition}
    \begin{itemize}
        \item Automatically called whenever an object is destroyed
        \begin{itemize}
            \item stack object goes out of scope
            \item heap object is deleted
        \end{itemize}
        \item Declared like a regular member function
        \item Name matches the class name prefixed with a tilde
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Destructors}
    \begin{columns}[onlytextwidth, t]
        \begin{column}{.5\textwidth}
            \begin{itemize}
                \item Header file:
                \begin{lstlisting}[basicstyle=\footnotesize]
class ellipse {
    public:
        ~ellipse();
        ...
                \end{lstlisting}
            \end{itemize}
        \end{column}
        \begin{column}{.5\textwidth}
            \begin{itemize}
                \item Source file:
                \begin{lstlisting}[basicstyle=\footnotesize]
ellipse::~ellipse()
{
}
                \end{lstlisting}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}


\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Destructors}
    \begin{itemize}
        \item Cannot take arguments nor return a value
        \item Can be called explicitely on pointer objects
        \begin{itemize}
            \item Destroys the object \textbf{without} releasing the memory
            \item Should when it was created with an overloaded new
            \begin{lstlisting}[basicstyle=\footnotesize]
void *b = malloc(sizeof(ellipse));
ellipse a = new(b) ellipse();
a->~ellipse();
free(b);
            \end{lstlisting}
            \item Should \textbf{never} be done on a non-pointer object as the destructor then risks being called twice
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Structs and unions}
    \begin{itemize}
        \item Classes can also be defined with the keywords \textit{struct} or \textit{union}
        \begin{itemize}
            \item structs are usually used to represent Plain Old Data (POD) types
            \item unions are used for special scenarios since they can only store one data member
        \end{itemize}
        \item default access specifier for unions and structs is public
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{this}
    \begin{itemize}
        \item Represents a pointer to the object whose member function is being executed
        \begin{lstlisting}
bool ellipse::ellipse is_it_me(ellipse &other)
{
    return (&other == this);
}
        \end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{this}
    \begin{itemize}
        \item *this returns a reference to the current object and can be used in chaining
        \begin{lstlisting}[basicstyle=\footnotesize]
ellipse &ellipse::draw() {
    // draw the ellipse some way or another
    return *this;
}

ellipse &ellipse::set_axes(int major, int minor) {
    m_major = major;
    m_minor = minor;
    return *this;
}

void main() {
    ellipse().set_axes(10, 5).draw().set_axes(5, 5).draw();
}
        \end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Static member variables}
    \begin{itemize}
        \item common for all objects of that class
        \begin{lstlisting}
class some_class {
    private:
        static int m_counter;
};
        \end{lstlisting}
        \item initialized outside the class
        \begin{lstlisting}
int some_class::m_counter = 0;
        \end{lstlisting}
        \item can be referred to as a member of any object or by the class name itself
        \begin{lstlisting}
std::cout << some_class::m_counter << "\n";
some_class a;
std::cout << a.m_counter << "\n";
        \end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Static members functions}
    \begin{itemize}
        \item can't use 'this' or access any non static members
        \item can access private variables
        \item commonly used for singleton classes
        \begin{lstlisting}[basicstyle=\footnotesize]
class some_class {
    public:
        static some_class *instance() {
            if (m_instance == NULL) {
                m_instance = new some_class();
            }
            return m_instance;
        }
    private:
        static some_class *m_instance;
        some_class() {}
};
        \end{lstlisting}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Const member functions}
    \begin{itemize}
        \item objects or references with a const qualifiers can only access read-only data
        \item constructor is still called and can modify data
        \item only const member functions can be called from a const object
        \item non-const objects can access const and non-const functions
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Const member functions}
    \lstinputlisting[firstline=3, basicstyle=\footnotesize]{code/const_member_functions.cpp}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Const member functions}
    \begin{itemize}
        \item cannot modify non-static non-mutable data members nor call non-const member functions
        \lstinputlisting[firstline=3, basicstyle=\footnotesize]{code/const_mutable.cpp}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Exercise 1}
    \begin{itemize}
        \item Rework the point class:
        \begin{itemize}
            \item refactor the existing constructor to use member init instead of statement init
            \item add the possibility to construct without coordinates (set them to 0 in that case)
            \item make sure accessors use the appropriate constness
            \item add a static method count() to the class which gives the number of points currently existing
            \begin{itemize}
                \item make sure the counter is updated by the constructor AND the destructor
            \end{itemize}
            \item add a static method distance() which gives the distance between 2 points (using again the appropriate constness)
            \begin{lstlisting}[basicstyle=\footnotesize]
float distance = sqrt((xa-xb)*(xa-xb) + (ya-yb)*(ya-yb))
            \end{lstlisting}
        \end{itemize}
    \end{itemize}
\end{frame}

%------------------------------------------------

\section{Special class member functions}
\makecurrentsection

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{What are we even talking about ?}
    \begin{itemize}
        \item Automatically compiler generated functions
        \begin{itemize}
            \item public
            \item non-virtual
            \item are called for every base class or member of the object
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Conditions of generation}
    \begin{itemize}
        \item default constructor
        \begin{itemize}
            \item if no other constructor explicitly declared
        \end{itemize}
        \item copy constructor
        \begin{itemize}
            \item if not declared
        \end{itemize}
        \item copy assignment operator
        \begin{itemize}
            \item if not declared
        \end{itemize}
        \item destructor
        \begin{itemize}
            \item if not declared
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Generated functions signatures}
    \begin{itemize}
        \item default constructor
        \begin{lstlisting}
    ellipse();
        \end{lstlisting}
        \item copy constructor
        \begin{lstlisting}
    ellipse(const ellipse &other);
        \end{lstlisting}
        \item copy assignment operator
        \begin{lstlisting}
    ellipse &operator=(const ellipse &other);
        \end{lstlisting}
        \item destructor
        \begin{lstlisting}
    ~ellipse();
        \end{lstlisting}
    \end{itemize}
\end{frame}

%------------------------------------------------

\section{For the next session !}
\makecurrentsection

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \textbf{Exercise 2}
    \begin{itemize}
        \item Create a line class which has:
        \begin{itemize}
            \item 2 private point members
            \item a constructor which takes 2 sets of coordinates
            \item a constructor which takes 2 points
            \item accessors for the 2 point objects (constness!)
            \item a function which gives the distance between the points of the line
        \end{itemize}
    \end{itemize}
\end{frame}

%------------------------------------------------

\section{References}
\makecurrentsection

%------------------------------------------------

\begin{frame}[fragile]
    \frametitle{\currentname}
    \begin{itemize}
        \item C++ Super-FAQ (isocpp.org/faq)
        \item The C++ Programming Language, Bjarne Stroustrup
        \item Thinking in C++, Bruce Eckel
        \item Effective C++, Scott Meyers
        \item Modern C++: Generic programming and design patterns applied, Andrei Alexandrescu
    \end{itemize}
\end{frame}

