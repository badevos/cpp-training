class ellipse {
    public:
        double area();
        double perimeter();

        double m_major, m_minor;
};
