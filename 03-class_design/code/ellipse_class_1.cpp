#include "ellipse_class_1.hpp"
#include <cmath>

ellipse::ellipse(int major, int minor) {
    m_major = major;
    m_minor = minor;
}

double ellipse::area() {
    return M_PI*(m_major/2)*(m_minor/2);
}

double ellipse::perimeter() {
    return 2*M_PI *sqrt(((m_major*m_major)+(m_minor*m_minor))/2);
}
