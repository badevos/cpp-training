#pragma once

class factory {
    private:
        factory() {}
        factory(const factory &other) {}
        factory &operator=(const factory &other) {}

    public:
        static factory *make();
};

