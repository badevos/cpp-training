#include "factory.h"

factory *factory::make()
{
    return new factory();
}

int main()
{
    //factory *fa = new factory();
    //factory fb;
    
    factory *fc = factory::make();
    //factory fd = *fc;

    factory *fe = factory::make();
    //*fe = *fc;
    
    delete fc;
    delete fe;
}

