#include <iostream>

class circle {
    public:
        circle(double rad) : m_rad(rad), m_call_count(0) {}
        double rad() const { m_call_count++; return m_rad; }
    private:
        double m_rad;
        mutable long m_call_count;
};

int main() {
    const circle c(10);
    std::cout << c.rad() << '\n';
}
