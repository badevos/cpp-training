#include "point.h"

#include <string>
#include <sstream>
#include <iostream>
#include <cmath>

unsigned long point::ms_count = 0;

point::point(float x, float y)
    : m_x(x), m_y(y)
{
    ms_count++;
}

point::~point()
{
    ms_count--;
}

const double point::distance(const point &a, const point &b)
{
    return sqrt((a.x()-b.x())*(a.x()-b.x()) + (a.y()-b.y())*(a.y()-b.y()));
}

void point::move(const float &dx, const float &dy)
{
    m_x += dx;
    m_y += dy;
}

std::string point::to_str()
{
    std::ostringstream ss;
    ss << "(" << m_x << ", " << m_y << ")";
    return ss.str();
}

int main()
{
    point a(5.9, 2.3);
    std::cout << "a = " << a.to_str() << "\n";
    a.move(1, 3);
    std::cout << "a = " << a.to_str() << "\n";

    point b(10, 0);
    point c(15, 0);
    std::cout << "b = " << b.to_str() << "\n";
    std::cout << "c = " << c.to_str() << "\n";
    std::cout << "dist(b, c) = " << point::distance(b, c) << "\n";

    return 0;
}

