#pragma once

#include <string>

class point {
    public:
        point(float x, float y);

        float x() { return m_x; }
        void set_x(float x) { m_x = x; }

        float y() { return m_y; }
        void set_y(float y) { m_y = y; }

        void move(float dx, float dy);
        std::string to_str();

    private:
        float m_x, m_y;
};

