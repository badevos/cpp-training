#include "point.h"

#include <string>
#include <sstream>
#include <iostream>

point::point(float x, float y)
{
    m_x = x;
    m_y = y;
}

void point::move(float dx, float dy)
{
    m_x += dx;
    m_y += dy;
}

std::string point::to_str()
{
    std::ostringstream ss;
    ss << "(" << m_x << ", " << m_y << ")";
    return ss.str();
}

int main()
{
    point a(5.9, 2.3);
    std::cout << "a = " << a.to_str() << "\n";
    a.move(1, 3);
    std::cout << "a = " << a.to_str() << "\n";
    
    return 0;
}

