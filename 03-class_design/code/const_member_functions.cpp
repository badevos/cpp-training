#include <iostream>

class ellipse {
    public:
        ellipse(double maj, double min) : m_maj(maj), m_min(min) {}
        double maj() const { return m_maj; }
        double m_min;
    private:
        double m_maj;
};

int main() {
    const ellipse a(10, 5);
    // a.m_min = 8; // error: m_minor cannot be modified
    std::cout << a.maj() << a.m_min;
}
