#include "line.h"
#include "point.h"

#include <iostream>

line::line(double ax, double ay, double bx, double by)
    : m_a(ax, ay), m_b(bx, by)
{
}

line::line(point a, point b)
    : m_a(a), m_b(b)
{
}

const double line::distance_a_b()
{
    return point::distance(m_a, m_b);
}

int main()
{
    point a(10, 0);
    point b(15, 0);
    std::cout << "a = " << a.to_str() << "\n";
    std::cout << "b = " << b.to_str() << "\n";

    line line0(a, b);
    std::cout << "line0 distance_a_b() = " << line0.distance_a_b() << "\n";

    line line1(10, 2, 20, 5);
    std::cout << "line1 distance_a_b() = " << line1.distance_a_b() << "\n";

    return 0;
}

