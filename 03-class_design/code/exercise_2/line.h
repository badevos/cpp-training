#pragma once

#include "point.h"

class line {
    public:
        line(double ax, double ay, double bx, double by);
        line(point a, point b);

        const point &a() const { return m_a; }
        void set_a(const point &a) { m_a = a; }
        const point &b() const { return m_b; }
        void set_b(const point &b) { m_b = b; }

        const double distance_a_b();

    private:
        point m_a;
        point m_b;
};

