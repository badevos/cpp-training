#pragma once

#include <string>

class point {
    public:
        point(float x = 0.0, float y = 0.0);
        ~point();

        static const unsigned long &count() { return ms_count; }
        static const double distance(const point &a, const point &b);

        const float &x() const { return m_x; }
        void set_x(const float &x) { m_x = x; }

        const float &y() const { return m_y; }
        void set_y(const float &y) { m_y = y; }

        void move(const float &dx, const float &dy);
        std::string to_str();

    private:
        static unsigned long ms_count;
        float m_x, m_y;
};

