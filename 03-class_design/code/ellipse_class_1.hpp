class ellipse {
    public:
        ellipse(int major, int minor);

        double area();
        double perimeter();

        double major() { return m_major; }
        double minor() { return m_minor; }

    private:
        double m_major, m_minor;
};

