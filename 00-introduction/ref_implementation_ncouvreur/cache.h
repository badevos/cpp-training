#pragma once

#include <string>

class cache {
    private:
        cache() : m_capacity(0) {}
        cache(const cache &other) {}
        cache &operator=(const cache &other) {}

    protected:
        int m_capacity;

    public:
        explicit cache(int capacity) : m_capacity(capacity) {}
        virtual ~cache() {}
        
        /**
         * get the value associated with the given key.
         * @param key The key to look for
         * @returns The value on cache hit, an empty string on cache miss.
         */
        virtual std::string get(const std::string &key) = 0;
        
        /**
         * set the key/value pair given as arguments into the cache.
         * @param key The key associated with the given values.
         * @param value The value associated with the given key.
         */
        virtual void set(const std::string &key, const std::string &value) = 0;
};

