#include "lru_cache.h"
#include <algorithm>


std::string lru_cache::get(const std::string &key)
{
    CacheCompararor comp(key);

    std::list<T_CacheEntry>::iterator it = std::find_if(m_list.begin(), m_list.end(), comp);

    if (it == m_list.end()) {
        return std::string();
    }
    else {
        std::string ret = it->second;
        m_list.push_front(*it);
        m_list.erase(it);
        return ret;
    }
}

void lru_cache::set(const std::string &key, const std::string &value)
{
    T_CacheEntry tmp(key, value);

    if (get(key).length() == 0) {
        m_size++;
    }
    else {
        m_list.remove(tmp);
    }

    m_list.push_front(tmp);

    if (m_size > m_capacity) {
        m_list.pop_back();
    }
}
