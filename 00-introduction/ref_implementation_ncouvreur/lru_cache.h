#pragma once

#include <list>
#include <utility>
#include "cache.h"


class lru_cache : public cache
{

public:

    lru_cache(int capacity) : cache(capacity), m_size(0) {};
    std::string get(const std::string &key);
    void set(const std::string &key, const std::string &value);


private:
    typedef std::pair<std::string, std::string> T_CacheEntry;

    struct CacheCompararor {
        CacheCompararor(const std::string &key) : m_key(key) {}
        bool operator ()(const T_CacheEntry &entry) {
            return entry.first == m_key;
        }

        std::string m_key;
    };

    std::list<T_CacheEntry> m_list;
    int m_size;

};
