#include "lru_cache.h"

#include <iostream>
#include <string>

int main()
{
    std::cout << "--- lru cache implementation tests ---\n";

    // test code :

    lru_cache cache(3);
    cache.set("id1", "1");
    cache.set("id2", "2");
    cache.set("id3", "3");
    cache.set("id4", "4");

    std::string id;
    id = cache.get("id1");
    std::cout << "id1 (expected: '')  = " << id << "\n";

    id = cache.get("id2");
    std::cout << "id2 (expected: '2') = " << id << "\n";

    id = cache.get("id3");
    std::cout << "id3 (expected: '3') = " << id << "\n";

    id = cache.get("id4");
    std::cout << "id4 (expected: '4') = " << id << "\n";

    // refresh id2 and add id5
    id = cache.get("id2");
    cache.set("id5", "5");

    id = cache.get("id1");
    std::cout << "id1 (expected: '')  = " << id << "\n";

    id = cache.get("id2");
    std::cout << "id2 (expected: '2') = " << id << "\n";

    id = cache.get("id3");
    std::cout << "id3 (expected: '') = " << id << "\n";

    id = cache.get("id4");
    std::cout << "id4 (expected: '4') = " << id << "\n";

    id = cache.get("id5");
    std::cout << "id4 (expected: '5') = " << id << "\n";


    std::cout << "--------------------------------------\n";
    return 0;
}

