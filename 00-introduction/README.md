# Introduction
You are tasked to implement an LRU cache mechanism to serve requests coming from clients wishing to use a build server.
```
                          +-> database
                          |
clients --> build_server --
                          |
                          +-> cache
```

The database contains identification tokens mapping a file to its unique identifier in the system. The build servers need those tokens to properly identify a file and its dependencies to ensure that only the smallest portion of the code is recompiled when a client modifies something.
Although the database is very comprehensive it acts as a severe bottleneck in this system and caches need to be implemented to optimize the latency of the requests coming from the build servers.

# Cache policy
The chosen cache policy is called LRU (for least recently used). Simply put: the least recently used items are discarded first.

As an example, if a cache with a capacity of 3 keys has the following state (from least recently to most recently used):
```
1 3 2
```

If a request for key 2 comes (called a cache "hit", since the key is in the cache), the new state of the cache would be:
```
2 1 3
```

Now if a request comes for key 5 (a cache "miss"), then the new state would be:
```
5 2 1
```

Key 3 is discarded since it was the least recently used and the cache cannot handle more than 3 keys.

# Project overview
Using c++, you should develop an LRU cache class which implements the cache interface described in the file "cache.h".
The capacity of the cache is passed as a constructor argument; you can access it as a protected data member.

Using our cache policy:

+ set() should update the value of the key if present, or add it to the cache if not, discard the least recently used key if at max capacity, then set the new key as the most recently used.
+ get() should obtain the value for the given key if present, then set that key as the most recently used, or return an empty string if not.

Your class declaration should be done in "lru_cache.h" and the implementation in "lru_cache.cpp".

A "main.cpp" file is provided with example tests that should run if your class implementation is valid.

To build your project just type "make" in the project folder; to clean all objects type "make clean".

