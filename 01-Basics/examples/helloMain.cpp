// Hello padowan!
#include <iostream>

int main()
{
  const std::string firstName = "FIRST NAME";
  const std::string familyName = "FAMILY_NAME";
  std::cout << "Hello padowan " << firstName << " " << familyName << std::endl;
}

